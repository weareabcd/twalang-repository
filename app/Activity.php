<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $primaryKey = 'id_activity';
    protected $table = 'activities';
    protected $fillable = ['nama_activity', 'deskripsi', 'time_start', 'foto'];

    public function experience(){
        return $this -> belongsTo('App\Experience', 'id_experience', 'id_experience');
    }
}

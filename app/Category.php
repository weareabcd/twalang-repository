<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'id_category';
    protected $table = 'categories';

    public function experiences(){
        return $this -> hasMany('App\Experience', 'id_category', 'id_category');
    }
}

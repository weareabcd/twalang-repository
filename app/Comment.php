<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $primaryKey = 'id_comment';
    protected $table = 'comments';

    public function user(){
        return $this -> belongsTo('App\User', 'id_user', 'id');
    }

    public function experience(){
        return $this -> belongsTo('App\Experience', 'id_experience', 'id_experience');
    }
}

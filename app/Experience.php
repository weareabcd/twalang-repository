<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $primaryKey = 'id_experience';
    protected $table = 'experiences';
    protected $fillable = ['nama_experience', 'deskripsi', 'max_orang', 'harga', 'id_location', 'id_category', 'id_host', 'address', 'time_start', 'time_end'];

    public function activities(){
        return $this -> hasMany('App\Activity', 'id_experience', 'id_experience');
    }
    
    public function comments(){
        return $this -> hasMany('App\Comment', 'id_experience', 'id_experience');
    }

    public function location(){
        return $this -> belongsTo('App\Location', 'id_location', 'id_location');
    }

    public function category(){
        return $this -> belongsTo('App\Category', 'id_category', 'id_category');
    }

    public function host(){
        return $this -> belongsTo('App\Host', 'id_host', 'id_host');
    }

    public function users(){
        return $this -> belongsToMany('App\User', 'users_memesan_experiences', 'id_experience', 'id_user');
    }
    public function experience_photos(){
        return $this -> hasMany('App\ExperiencePhoto', 'id_experience', 'id_experience');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction', 'experience_id', 'id_experience');
    }
}

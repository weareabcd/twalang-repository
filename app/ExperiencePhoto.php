<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperiencePhoto extends Model
{
    protected $table = 'experience_photos';
    protected $fillable = ['id_experience', 'link'];

    public function experience(){
        return $this->belongsTo('App\Experience', 'id_experience', 'id_experience');
    }
}

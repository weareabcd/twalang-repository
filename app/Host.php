<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    protected $primaryKey = 'id_host';
    protected $table = 'hosts';

    public function user(){
        return $this -> belongsTo('App\User', 'id_user', 'id');
    }

    public function experiences(){
        return $this -> hasMany('App\Experience', 'id_host', 'id_host');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction', 'host_id');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Experience;
use App\Activity;
use App\Category;
use App\Transaction;
use App\User;
use App\Location;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(){
        $nUsers = count(User::whereIn('role', ['host', 'guest'])->get());
        $nHosts = count(User::whereIn('role', ['host'])->get());
        $nExperiences = count(Experience::all());
        $nPendingTransactions = count(Transaction::where('status', 'pending')->get());
        $locationByTransaction = DB::select("SELECT b.nama_location 'nama_location', COUNT(*) 'n_transaksi' FROM (SELECT locations.nama_location, a.* FROM locations
        JOIN (SELECT transactions.transaction_id, experiences.nama_experience, experiences.id_location FROM transactions JOIN experiences
        ON transactions.experience_id = experiences.id_experience) a
        ON locations.id_location = a.id_location) b GROUP BY b.id_location ORDER BY COUNT(*) DESC");
        
        $locationByExperience = DB::select("SELECT b.nama_location 'nama_location', COUNT(*) 'n_experiences' FROM (SELECT locations.nama_location, experiences.* FROM locations
        JOIN experiences
        ON locations.id_location = experiences.id_location) b GROUP BY b.id_location ORDER BY COUNT(*) DESC");

        $totalGrossAllTransactions = Transaction::where('status', 'Success')->sum('gross_total');

        $jenisExperienceTerbanyak = DB::table('transactions')
            ->join('experiences', 'transactions.experience_id', '=', 'experiences.id_experience')
            ->Join('categories', 'categories.id_category', '=', 'experiences.id_category')   
            ->select('categories.nama_category', DB::raw('COUNT(categories.id_category) as total'))
            ->groupBy('categories.id_category', 'categories.nama_category')
            ->where('status', '=', 'Success')
            ->orderBy(DB::raw('COUNT(transactions.experience_id)'),'desc')
            ->distinct()
            ->get();

        $namaJenis = [];
        $totalJenis = [];
        foreach($jenisExperienceTerbanyak as $jenis){
            $namaJenis[] = $jenis->nama_category;
            $totalJenis[] = $jenis->total;
        }
        // dd($totalJenis);
        return view('admin.dashboard', [
            'nUsers' => $nUsers,
            'nHosts' => $nHosts,
            'nExperiences' => $nExperiences,
            'namaJenis' => $namaJenis,
            'nPendingTransactions' => $nPendingTransactions,
            'locationByTransaction' => $locationByTransaction,
            'totalJenis' => $totalJenis,
            'locationByExperience' => $locationByExperience,
            'totalGrossAllTransactions' => $totalGrossAllTransactions,
        ]);
    }

    public function kelola_user(){
        $users = User::all();
        return view('admin.kelola_user', compact('users'));
    }

    public function kelola_transaksi()
    {
     
        //Call Transaction
        $transactions = \App\Transaction::all();
        $pendings = \App\Transaction::where('status','pending')->get();
        $success_payments = \App\Transaction::where('status','success')->get();
        $expireds = \App\Transaction::where('status','expired')->get();
        $faileds = \App\Transaction::where('status','failed')->get();

        //Hitung Nilai Transaksi
        $nilai_transaksi = 0;
        foreach( $pendings as $pdg){
            $nilai_transaksi += $pdg->gross_total;
        }

        $nilai_berhasil = 0;
        foreach( $success_payments as $scs ){
            $nilai_berhasil += $scs->gross_total;
        }

        $nilai_transaksi += $nilai_berhasil;
        
        return view('admin.transaction',compact('transactions','pendings','nilai_transaksi','nilai_berhasil','success_payments','expireds','faileds'));

    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

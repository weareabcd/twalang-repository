<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Experience;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ExperienceActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Experience $experience)
    {
        $activity = new Activity();
        $activity->nama_activity = $request->nama_activity;
        $activity->deskripsi = $request->deskripsi;
        $activity->time_start = $request->time_start;
        $activity->id_experience = $experience->id_experience;

        $status = "Menambah Aktivitas Berhasil!";
        //UPLOAD FOTO
        $image = $request->file("thing");
        if($image!=null){
            $data = $image->store("1IsDL0EzWK4z6rj1l1fbyYX65hE7pxwCu", "google");
            $contents = collect(Storage::disk('google')->listContents('1IsDL0EzWK4z6rj1l1fbyYX65hE7pxwCu', false));
            $dir = $contents->where('type', '=', 'file')
                ->where('name', '=', basename($data))
                ->first();
            if ( ! $dir) {
                $link =  'File gagal di Upload!';
            } else{
                $link="https://drive.google.com/uc?id=".$dir['basename'];
            }
            if($link!='File gagal di Upload!'){
                $activity->foto = $link;
            }else{
                $status = $status." Tapi Gagal Mengupload Foto!";
            }
        }

        $activity->save();

        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience, Activity $activity)
    {
        $activity->update([
            'nama_activity' => $request->nama_activity,
            'deskripsi' => $request->deskripsi,
            'time_start' => $request->time_start,
        ]);
        $status = 'Mengedit Aktivitas Berhasil!';

        //UPLOAD FOTO
        $image = $request->file("thing");
        if($image!=null){
            $data = $image->store("1IsDL0EzWK4z6rj1l1fbyYX65hE7pxwCu", "google");
            $contents = collect(Storage::disk('google')->listContents('1IsDL0EzWK4z6rj1l1fbyYX65hE7pxwCu', false));
            $dir = $contents->where('type', '=', 'file')
                ->where('name', '=', basename($data))
                ->first();
            if ( ! $dir) {
                $link =  'File gagal di Upload!';
            } else{
                $link="https://drive.google.com/uc?id=".$dir['basename'];
            }
            if($link!='File gagal di Upload!'){
                $activity->update([
                    'foto' => $link
                ]);
            }else{
                $status = $status." Tapi Gagal Mengupload Foto!";
            }
        }
        
        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', $status );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience, Activity $activity)
    {
        $activity->delete($activity->id_activity);
        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', 'Menghapus Aktivitas Berhasil!');
    }
}

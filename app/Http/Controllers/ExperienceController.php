<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Experience;
use \App\Location;
use \App\Category;
use App\Transaction;
use Carbon\Carbon;


class ExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locations = Location::all();
        $categories = Category::all();
        return view('experiences.create', compact('locations', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
    //    if($request->hasfile('link'))
    //     {
    //         $image=  $request->file('link');
            
    //             $data=$request->na.'.'.$image->getClientOriginalExtension();
    //         Storage::disk('google')->put('/'.$data, file_get_contents($request->file('link')));
    //         $contents = collect(Storage::disk('google')->listContents('/', false));
    //         $dir = $contents->where('type', '=', 'file')
    //             ->where('name', '=', $data)
    //             ->first(); // There could be duplicate directory names!
    //         if ( ! $dir) {
    //             return 'File gagal di Upload!';
    //         } 
    //         $request->link="https://drive.google.com/open?id=".$dir['basename'];
    //     }
        $experience = new Experience();
        $experience->nama_experience = $request->nama_experience;
        $experience->deskripsi = $request->deskripsi;
        $experience->max_orang = $request->max_orang;
        $experience->time_start = $request->time_start;
        $experience->time_end = $request->time_end;
        $experience->harga = $request->harga;
        $experience->id_location = $request->id_location;
        $experience->id_category = $request->id_category;
        $experience->id_host = $request->id_host;
        $experience->address = $request->address;
        $experience->save();
        
        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', 'Berhasil menambah petualangan baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        //

        $similars = \App\Experience::where('id_category',$experience->id_category)->get();
        $today = Carbon::now();
        $defaultInput = $today->format('Y') . '-' . $today->format('m') . '-' . $today->format('d');

        $sum = 0;
        $n = 0;
        foreach ($experience->transactions as $transaction) {
            if(!is_null($transaction->review_rating)){
                $sum += $transaction->review_rating;
                $n++;
            }
        }
        $avg = $sum/($n?$n:1);
        $avg = intval($avg);

        return view('experiences.show', compact('experience','similars','today','defaultInput', 'avg'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rate(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        $transaction->review_rating = $request->review_rating;
        $transaction->review_description = $request->review_description;
        $transaction->save();

        return redirect()->back()->with('status', 'Berhasil mengirim review');
    }

}

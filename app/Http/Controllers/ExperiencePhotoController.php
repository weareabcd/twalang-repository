<?php

namespace App\Http\Controllers;

use App\Experience;
use App\ExperiencePhoto;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ExperiencePhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function upload(Request $request){
        $image = $request->file("thing");
        dd($image);
        $data = $image->store("1I_KPKOaC9toJjGEMwvLzWbpNynADKfV3", "google");
        $contents = collect(Storage::disk('google')->listContents('1I_KPKOaC9toJjGEMwvLzWbpNynADKfV3', false));
        $dir = $contents->where('type', '=', 'file')
            ->where('name', '=', basename($data))
            ->first(); // There could be duplicate directory names!
        if ( ! $dir) {
            return 'File gagal di Upload!';
        } else{
            $link="https://drive.google.com/uc?id=".$dir['basename'];
            return $link;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Experience $experience)
    {
        $image = $request->file("thing");
        $data = $image->store("1I_KPKOaC9toJjGEMwvLzWbpNynADKfV3", "google");
        $contents = collect(Storage::disk('google')->listContents('1I_KPKOaC9toJjGEMwvLzWbpNynADKfV3', false));
        $dir = $contents->where('type', '=', 'file')
            ->where('name', '=', basename($data))
            ->first();
        if ( ! $dir) {
            $link =  'File gagal di Upload!';
        } else{
            $link="https://drive.google.com/uc?id=".$dir['basename'];
        }
        if($link!='File gagal di Upload!'){
            $foto = new ExperiencePhoto();
            $foto->link = $link;
            $foto->id_experience = $experience->id_experience;
            $foto->save();
            $status = "Menambahkan Foto Petualangan Berhasil!";
        }else{
            $status = "Menambahkan Foto Petualangan Gagal!";
        }

        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', $status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExperiencePhoto  $experiencePhoto
     * @return \Illuminate\Http\Response
     */
    public function show(ExperiencePhoto $experiencePhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExperiencePhoto  $experiencePhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(ExperiencePhoto $experiencePhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExperiencePhoto  $experiencePhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExperiencePhoto $experiencePhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExperiencePhoto  $experiencePhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience,ExperiencePhoto $experience_photo)
    {
        $experience_photo->delete();
        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', 'Menghapus Foto Petualangan Berhasil!');
    }
}

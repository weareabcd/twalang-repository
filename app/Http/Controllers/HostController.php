<?php

namespace App\Http\Controllers;

use App\Category;
use App\Experience;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Host;
use App\User;
use App\Location;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hosts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->id_user);
        $host = new Host();
        $host->id_user = $request->id_user;
        $host->nama_host = $request->nama_host;
        //FOTO HOST
        $host->save();
        $user->update([
            'role' => 'host'
        ]);
        return redirect('/experiences/create')->with('status', 'Anda berhasil menjadi Host!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getYears(){
        $transactions = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success');
        $years = [];
        foreach($transactions as $transaction){
            $tanggalTransaction = new Carbon( $transaction->book_date );
            $years[] = $tanggalTransaction->year;
        }
        if(sizeof($years) == 0){
            $years[] = now()->year;
        }
        sort($years);
        
        $currentYear = '';
        foreach($years as $year){
            if($currentYear != $year){
                $currentYear = $year;
                $shortenYears[] = $currentYear;
            }
        }
        if(count($shortenYears) <= 5 ){
            if(count($shortenYears) == 0){
                $firstYear = Carbon::now()->year;
            }else{
                $firstYear = $shortenYears[0];
            }
            $n = count($shortenYears);
            for($i = 0; $i < (5 - $n); $i++){
                $firstYear -= 1;
                array_unshift($shortenYears, $firstYear);
            }
        }
        return $shortenYears;
    }

    public function dashboard()
    {
        $time = 'all';
        $nDoneTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '<=' , Carbon::now())->get());
        $nSoonTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>' , Carbon::now())->get());
        $nNotPaid = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->get());

        $xAxis = $this->getYears();
        $yAxis = array();
        for($i = 0; $i < count($xAxis); $i++){
            $yAxis[] = count(Transaction::where('host_id', Auth::user()->host->id_host)->whereYear('created_at', $xAxis[$i])->where('status', 'Success')->get());
        }

        $income = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->sum('gross_total');
        $willIncome = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->sum('gross_total');

        $inPrevYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year - 1)->sum('gross_total');
        $inThisYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year)->sum('gross_total');
        $inDiff = $inThisYear - $inPrevYear;
        $diffPercent = ($inDiff / ($inPrevYear ? $inPrevYear : 1)) * 100;

        $categoryByTransaction = DB::select("SELECT b.nama_category 'nama_category', COUNT(*) 'n_transaksi' FROM (SELECT categories.nama_category, a.* FROM categories
        JOIN (SELECT transactions.transaction_id, experiences.nama_experience, experiences.id_category FROM transactions JOIN experiences
        ON transactions.experience_id = experiences.id_experience) a
        ON categories.id_category = a.id_category) b GROUP BY b.id_category ORDER BY COUNT(*) DESC");

        $upcomingExperiences = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>=' , Carbon::today())->orderBy('book_date')->get();
        
        return view('hosts.dashboard', [
            'nDoneTransactions' => $nDoneTransactions,
            'nSoonTransactions' => $nSoonTransactions,
            'nNotPaid' => $nNotPaid,
            'income' => $income,
            'willIncome' => $willIncome,
            'diffPercent' => $diffPercent,
            'xAxis' => $xAxis,
            'yAxis' => $yAxis,
            'time' => $time,
            'categoryByTransaction' => $categoryByTransaction,
            'upcomingExperiences' => $upcomingExperiences
        ]);
    }
    
    public function dashboardSortByTime($time){
        $categoryByTransaction = DB::select("SELECT b.nama_category 'nama_category', COUNT(*) 'n_transaksi' FROM (SELECT categories.nama_category, a.* FROM categories
        JOIN (SELECT transactions.transaction_id, experiences.nama_experience, experiences.id_category FROM transactions JOIN experiences
        ON transactions.experience_id = experiences.id_experience) a
        ON categories.id_category = a.id_category) b GROUP BY b.id_category ORDER BY COUNT(*) DESC");

        $upcomingExperiences = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>=' , Carbon::today())->orderBy('book_date')->get();

        if($time == 'thisweek'){
            $nDoneTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereDate('book_date', '<=' , Carbon::now())->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get());
            $nSoonTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>' , Carbon::now())->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get());
            $nNotPaid = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->get());
            $thisMonth = Carbon::now()->month;
            $xAxis = []; 
            $yAxis = [];
            for($i=0; $i < 7; $i++) {
                array_unshift($xAxis, Carbon::now()->subDays($i)->format('D'));
                array_unshift($yAxis, count(Transaction::where('host_id', Auth::user()->host->id_host)->where('created_at', Carbon::today()->subDays($i))->where('status', 'Success')->get()));
            }

            $income = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('gross_total');
            $willIncome = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('gross_total');

            $inPrevWeek = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereBetween('created_at', [Carbon::today()->subDays(13), Carbon::today()->subDays(7)])->sum('gross_total');
            $inThisWeek = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereBetween('created_at', [Carbon::today()->subDays(6), Carbon::today()->subDays(0)])->sum('gross_total');
            $inDiff = $inThisWeek - $inPrevWeek;
            $diffPercent = ($inDiff / ($inPrevWeek ? $inPrevWeek : 1)) * 100;
        } else if($time == 'thismonth'){
            $nDoneTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereDate('book_date', '<=' , Carbon::now())->whereMonth('created_at', Carbon::now()->month)->get());
            $nSoonTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>' , Carbon::now())->whereMonth('created_at', Carbon::now()->month)->get());
            $nNotPaid = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereMonth('created_at', Carbon::now()->month)->get());
            $thisMonth = Carbon::now()->month;
            $xAxis = []; 
            $yAxis = []; 
            $today = today();
            for($i=1; $i < $today->daysInMonth + 1; ++$i) {
                $xAxis[] = Carbon::createFromDate($today->year, $today->month, $i)->format('D');
                $yAxis[] = count(Transaction::where('host_id', Auth::user()->host->id_host)->whereMonth('created_at', $thisMonth)->whereDay('created_at', $i)->where('status', 'Success')->get());
            }

            $income = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereMonth('created_at', Carbon::now()->month)->sum('gross_total');
            $willIncome = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereMonth('created_at', Carbon::now()->month)->sum('gross_total');

            $inPrevMonth = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereMonth('created_at', Carbon::today()->subMonths(1))->sum('gross_total');
            $inThisMonth = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereMonth('created_at', Carbon::now()->month)->sum('gross_total');
            $inDiff = $inThisMonth - $inPrevMonth;
            $diffPercent = ($inDiff / ($inPrevMonth ? $inPrevMonth : 1)) * 100;
        } else if($time == 'thisyear'){
            $nDoneTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereDate('book_date', '<=' , Carbon::now())->whereYear('created_at', Carbon::now()->year)->get());
            $nSoonTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>' , Carbon::now())->whereYear('created_at', Carbon::now()->year)->get());
            $nNotPaid = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereYear('created_at', Carbon::now()->year)->get());
            $xAxis = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];
            $yAxis = array();
            $thisYear = Carbon::now()->year;
            for($i = 0; $i < count($xAxis); $i++){
                $yAxis[] = count(Transaction::where('host_id', Auth::user()->host->id_host)->whereYear('created_at', $thisYear)->whereMonth('created_at', $i + 1)->where('status', 'Success')->get());
            }

            $income = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year)->sum('gross_total');
            $willIncome = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->whereYear('created_at', Carbon::now()->year)->sum('gross_total');

            $inPrevYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year - 1)->sum('gross_total');
            $inThisYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year)->sum('gross_total');
            $inDiff = $inThisYear - $inPrevYear;
            $diffPercent = ($inDiff / ($inPrevYear ? $inPrevYear : 1)) * 100;
        } else if($time == 'all'){
            $nDoneTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereDate('book_date', '<=' , Carbon::now())->get());
            $nSoonTransactions = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->where('book_date', '>' , Carbon::now())->get());
            $nNotPaid = count(Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->get());
            $xAxis = $this->getYears();
            $yAxis = array();
            for($i = 0; $i < count($xAxis); $i++){
                $yAxis[] = count(Transaction::where('host_id', Auth::user()->host->id_host)->whereYear('created_at', $xAxis[$i])->where('status', 'Success')->get());
            }

            $income = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->sum('gross_total');
            $willIncome = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'pending')->sum('gross_total');

            $inPrevYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year - 1)->sum('gross_total');
            $inThisYear = Transaction::where('host_id', Auth::user()->host->id_host)->where('status', 'Success')->whereYear('created_at', Carbon::now()->year)->sum('gross_total');
            $inDiff = $inThisYear - $inPrevYear;
            $diffPercent = ($inDiff / ($inPrevYear ? $inPrevYear : 1)) * 100;
        }
        return view('hosts.dashboard', [
            'nDoneTransactions' => $nDoneTransactions,
            'nSoonTransactions' => $nSoonTransactions,
            'nNotPaid' => $nNotPaid,
            'income' => $income,
            'willIncome' => $willIncome,
            'diffPercent' => $diffPercent,
            'xAxis' => $xAxis,
            'yAxis' => $yAxis,
            'time' => $time,
            'categoryByTransaction' => $categoryByTransaction,
            'upcomingExperiences' => $upcomingExperiences
        ]);
    }

    public function transaction()
    {

        $host = \App\User::find(Auth::id());
        $now = Carbon::now();

        $pendings = \App\Transaction::where([
            ['host_id','=',$host->host->id_host],
            ['status','=','pending']
        ])->get();

        $success = \App\Transaction::where([
            ['host_id','=',$host->host->id_host],
            ['status','=','success']
        ])->get();

        $upcomings = \App\Transaction::where([
            ['host_id','=',$host->host->id_host],
            ['status','=','success'],
            ['book_date','>',$now]
        ])->get();

        $completeds = \App\Transaction::where([
            ['host_id','=',$host->host->id_host],
            ['status','=','success'],
            ['book_date','<',$now]
        ])->get();

        $transactions = \App\Transaction::where('host_id',$host->host->id_host)->get();

        //Counting Proyeksi Keuntungan
        $proyeksi_keuntungan = 0;
        foreach( $pendings as $pdn){
            $proyeksi_keuntungan += $pdn->gross_total;
        };

        //Counting Total Keuntungan
        $total_keuntungan = 0;
        foreach( $success as $scs){
                $total_keuntungan += $scs->gross_total;
        };

        //Counting Jumlah Tamu Terlayani
        $tamu_terlayani = 0;
        foreach( $completeds as $cmpltd){
            $tamu_terlayani += $cmpltd->amount;
        };

        return view('hosts.transaction',compact('pendings','proyeksi_keuntungan','total_keuntungan','tamu_terlayani','upcomings','completeds'));
    }

}

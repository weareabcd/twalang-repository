<?php

namespace App\Http\Controllers;

use App\Experience;
use App\Location;
use App\Category;
use Illuminate\Http\Request;


class HostExperienceController extends Controller
{
    public function index(){
        return view('hosts.experiences.index');
    }

    public function show(Experience $experience){
        $locations = Location::all();
        $categories = Category::all();
        return view('hosts.experiences.show', compact('experience', 'locations', 'categories'));
    }
    
    public function update(Request $request, Experience $experience){
        $experience->update(
            $request->all()
        );
        return redirect("/kelola_petualangan/show/$experience->id_experience")->with('status', 'Menyimpan Perubahan Berhasil!');
    }
}

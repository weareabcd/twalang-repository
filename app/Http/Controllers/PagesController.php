<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Carbon\Carbon;
use \App\Experience;
use App\Transaction;
use Illuminate\Support\Facades\Auth;


class PagesController extends Controller
{
    
    public function first()
    {
        return view('pre_host.1');
    }

    public function upcoming()
    {
        $today = Carbon::today();

        //Calling Experience Transaction
        $pendings = \App\Transaction::where([
            ['user_id','=',Auth::id()],
            ['status','=','pending']
        ])->get();

        $upcomings = \App\Transaction::where([
            ['user_id','=',Auth::id()],
            ['status','=','success'],
            ['book_date','>',$today]
        ])->get();

        $past  = \App\Transaction::where([
            ['user_id','=',Auth::id()],
            ['status','=','success'],
            ['book_date','<',$today]
        ])->get();
        
        return view('experiences.upcoming',compact('pendings','upcomings','past'));
    }
    

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use Carbon\Carbon;
use \App\Experience;
use Midtrans\Notification;
use Midtrans\Snap;
use Midtrans\Config;
use App\Transaction;
use Illuminate\Support\Facades\Auth;


class Payment extends Controller
{

    protected $request;
    protected $serverKey;
    
    public function __construct(Request $request)
    {
        $this->request = $request;

        Config::$serverKey = config('services.midtrans.serverKey');
        Config::$isProduction = config('services.midtrans.isProduction');
        config::$isSanitized = config('services.midtrans.isSanitized');
        config::$is3ds = config('services.midtrans.is3ds');
        
    }

    public function pay(Request $request)
    {
       
        //Creating Transaction
        $customer = User::find(Auth::id());
        $target = Experience::where('id_experience',$request->experience_id)->first();
        $total = $target->harga * $request->amount;
        
        $transaction = Transaction::create([

            'user_id' => $request->user_id,
            'host_id' => $target->id_host,
            'experience_id' => $request->experience_id,
            'book_date' => $request->tanggal,
            'amount' => $request->amount,
            'gross_total' => $total,
            'status' => "pending"

        ]);

        $transaction_details = array(
            'order_id' => $transaction->transaction_id,
            'gross_amount' => $transaction->gross_total,
        );

        $item1_details = array(
            'id' => $request->experience_id,
            'price' => $target->harga,
            'quantity' => $request->amount,
            'name' => $target->nama_experience 
        );

        $item_details = array(
            $item1_details
        );
        
        $customer_details = array(
            'first_name' => $customer->name,
            'email' => $customer->email,
            'phone' => $customer->no_telepon
        );

        $payment = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );

        $snapToken = Snap::getSnapToken($payment);

        $updateSnapToken = Transaction::find($transaction->transaction_id);
        $updateSnapToken->snap_token = $snapToken;
        $updateSnapToken->save();

        $dateTime = $request->tanggal;
        $tanggal = Carbon::parse($dateTime);

        return view('experiences.payment',compact('customer','target','request','total','tanggal','snapToken'));
    }

    public function inputTransaction(Request $request)
    {

        $target = Experience::where('id_experience',$request->experience_id)->first();
        $total = $target->harga * $request->amount;
        $transaction = Transaction::create([

            'user_id' => $request->user_id,
            'experience_id' => $request->experience_id,
            'book_date' => $request->book_date,
            'amount' => $request->amount,
            'gross_total' => $total,
            'status' => "pending"

        ]);


    }

    public function adminSetStatus(Request $request,$id)
    {
        $target = \App\Transaction::find($id)
        ->update([
             'status' => $request->status
         ]);

        return redirect('admin/transaction');
    }

}

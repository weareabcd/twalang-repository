<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Experience;
use App\Location;
use App\Category;

class SearchController extends Controller
{

    private $search_title;

    public function result(Request $request){

        $target = $request->input();
        $this->searchTitle($target);

        $locs = \App\Location::all();
        $cats = \App\Category::all();
        $locations = \App\Location::where('id_location',$target['kota'])->first();
        $categories = \App\Category::where('id_category',$target['jenis_petualangan'])->first();
        $search_title = $this->search_title;
        
        if( $target['keyword'] == null ){
            if ( $target['jenis_petualangan'] == null ){
                if ( $target['kota'] == null ){
                    $results = \App\Experience::all();
                    //return "Semua filter kosong";
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                } else {
                    $results = \App\Experience::where('id_location',$target['kota'])->paginate(10);
                    //return "Menampilkan semua Experience di " . $target['kota'];
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                }
            } else {
                if ( $target['kota'] == null ){
                    $results = \App\Experience::where('id_category',$target['jenis_petualangan'])->paginate(10);
                    //return "menampilkan " . $target['jenis_petualangan'] . "di SEMUA KOTA";
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                } else {
                    $results = \App\Experience::where([
                        ['id_category','=',$target['jenis_petualangan']],
                        ['id_location','=',$target['kota']]
                    ])->paginate(10);
                    //return "menampilkan " . $target['jenis_petualangan'] . " di " . $target['kota'];
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                }
            }
        } else {
            if ( $target['jenis_petualangan'] == null ){
                if ( $target['kota'] == null ){
                    $results = \App\Experience::where('nama_experience','LIKE',"%{$target['keyword']}%")->paginate(10);
                    //return "Menampilkan " . $target['keyword'] . " di SEMUA KATEGORI di SEMUA KOTA";
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                } else {
                    $results = \App\Experience::where([
                        ['nama_experience','LIKE',"%{$target['keyword']}%"],
                        ['id_location','=',$target['kota']]
                    ])->paginate(10);
                    //return "Menampilkan " . $target['keyword'] . " di semua Kategori di " . $target['kota'];
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                }
            } else {
                if ( $target['kota'] == null ){
                    $results = \App\Experience::where([
                        ['nama_experience','LIKE',"%{$target['keyword']}%"],
                        ['id_category','=',$target['jenis_petualangan']]
                    ])->paginate(10);
                    //return "menampilkan " . $target['keyword'] . " dengan jenis " . $target['jenis_petualangan'] . " di SEMUA KOTA";
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                } else {
                    $results = \App\Experience::where([
                        ['nama_experience','LIKE',"%{$target['keyword']}%"],
                        ['id_category','=',$target['jenis_petualangan']],
                        ['id_location','=',$target['kota']]
                    ])->paginate(10);
                    //return "menampilkan " . $target['keyword'] . " dengan jenis " . $target['jenis_petualangan'] . " di " . $target['kota'];
                    return view('search.result',compact('target','results','locations','categories','locs','cats','search_title'));
                }
            }
        }

        

        /*
        $locs = \App\Location::all();
        $cats = \App\Category::all();


        $locations = \App\Location::where('id_location',$target['kota'])->first();
        $categories = \App\Category::where('id_category',$target['jenis_petualangan'])->first();
        $results = \App\Experience::where([
            ['id_location','=',$target['kota']],
            ['id_category','=',$target['jenis_petualangan']] 
        ])->paginate(10);
        
        return view('search.result',compact('target','results','locations','categories','locs','cats'));
        */
    }

    public function searchTitle(array $input){

        if( $input['keyword'] != null)
        {
            $key = $input['keyword'];
        } else {
            $key = "apapun";
        }
        
        if( $input['jenis_petualangan'] != null)
        {
            $cat = \App\Category::where('id_category',$input['jenis_petualangan'])->first()->nama_category;
        } else {
            $cat = "semua kategori";
        }

        if( $input['kota'] != null)
        {
            $loc = \App\Location::where('id_location',$input['kota'])->first()->nama_location;
        } else {
            $loc = "semua kota";
        }

        $search_title = "Mencari " . $key . " di " . $loc . " dalam " . $cat;
        $this->search_title = $search_title;

    }

}

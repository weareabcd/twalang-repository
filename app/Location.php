<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $primaryKey = 'id_location';
    protected $table = 'locations';

    public function experiences(){
        return $this -> hasMany('App\Experience', 'id_location', 'id_location');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $primaryKey = 'transaction_id';

    protected $dates = [
        'book_date'
    ];
    
    protected $fillable = [
        'user_id','experience_id','book_date','amount','gross_total','status','host_id'
    ];

    public function experience()
    {
        return $this->belongsTo('App\Experience','experience_id','id_experience');
    }

    public function guest()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function host()
    {
        return $this->belongsTo('App\Host','host_id','id_host');
    }

}

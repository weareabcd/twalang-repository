<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPesanExperience extends Model
{
    protected $primaryKey = 'id_users_memesan_experiences';
    protected $table = 'users_memesan_experiences';
}

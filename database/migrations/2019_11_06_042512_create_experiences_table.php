<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiences', function (Blueprint $table) {
            $table->bigIncrements('id_experience');
            $table->string('nama_experience');            
            $table->string('deskripsi')->nullable();
            $table->unsignedBigInteger('max_orang')->nullable();  
            $table->dateTime('estimasi_durasi');
            $table->string('foto')->nullable();
            $table->unsignedBigInteger('harga');      
            $table->unsignedBigInteger('id_location');      
            $table->unsignedBigInteger('id_category');
            $table->unsignedBigInteger('id_host');
            
            $table->foreign('id_location')->references('id_location')->on('locations');
            $table->foreign('id_category')->references('id_category')->on('categories');
            $table->foreign('id_host')->references('id_host')->on('hosts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('experiences');
    }
}

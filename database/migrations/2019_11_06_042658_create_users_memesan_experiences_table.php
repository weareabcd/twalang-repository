<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersMemesanExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_memesan_experiences', function (Blueprint $table) {
            $table->bigIncrements('id_users_memesan_experiences');
            $table->string('kode_pemesanan');
            $table->date('tanggal_pesanan');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_experience');

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_experience')->references('id_experience')->on('experiences');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests_memesan_experiences');
    }
}

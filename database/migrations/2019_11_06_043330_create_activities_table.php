<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id_activity');
            $table->string('nama_activity');
            $table->string('destinasi')->nullable();
            $table->string('deskripsi')->nullable();
            $table->dateTime('estimasi_durasi');
            $table->string('foto')->nullable();
            $table->unsignedBigInteger('id_experience');

            $table->foreign('id_experience')->references('id_experience')->on('experiences');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}

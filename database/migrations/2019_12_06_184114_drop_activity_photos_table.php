<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropActivityPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('activity_photos');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('activity_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('link');
            $table->unsignedBigInteger('id_activity');

            $table->foreign('id_activity')->references('id_activity')->on('activities');
            $table->timestamps();
        });
    }
}

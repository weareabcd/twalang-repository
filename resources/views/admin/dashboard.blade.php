@extends('layouts.dashboard')

@section('content')
<div class="row mb-3">
    <div class="col-12">
        <h2><strong>Hai! Selamat Datang di Dasbor Admin!</strong></h2>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6 mb-3">
        {{-- jumlah pengguna disistem --}}
        <div class="card shadow">
            <div class="card-body text-center">
                <h3 class="mb-0">{{$nUsers}}</h3>
                <div>Pengguna</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
        {{-- jumlah host disistem --}}
        <div class="card shadow">
            <div class="card-body text-center">
                <h3 class="mb-0">{{$nHosts}}</h3>
                <div>Host</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
        {{-- jumlah petualangan disistem --}}
        <div class="card shadow">
            <div class="card-body text-center">
                <h3 class="mb-0">{{$nExperiences}}</h3>
                <div>Petualangan</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 mb-3">
        {{-- jumlah transaksi yang sedang pending --}}
        <div class="card shadow">
            <div class="card-body text-center">
                <h3 class="mb-0">{{$nPendingTransactions}}</h3>
                <div>Transaksi Menunggu</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 mb-3">
        {{-- grafik barchart kota terlaris --}}
        <div class="card shadow">
            <div class="card-body">
                <h5 class="mb-3">Grafik Kota Terlaris</h5>
                <div class="chart-bar">
                    <canvas id="myBarChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-3">
        {{-- grafik pie chart tema terlaris --}}
        <div class="card shadow mb-3">
            <div class="card-body">
                <h5 class="mb-3">Grafik Tema Terlaris</h5>
                <div class="chart-pie-twalang">
                    <canvas id="myPieChart"></canvas>
                    <div class="mt-4 text-center small">
                        <span class="mr-2">
                            <i class="fas fa-circle text-primary"></i> Wisata Air
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-success"></i> Pertanian
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle text-info"></i> Tutorial
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle" style="color:red"></i> Wisata Malam
                        </span>
                        <span class="mr-2">
                            <i class="fas fa-circle" style="color:yellow"></i> Kuliner
                        </span>
                    </div>
                </div>
            </div>
        </div>
        {{-- jumlah semua pendapatan di transaksi --}}
        <div class="card shadow">
            <div class="card-body text-center">
                <h3 class="mb-0">@currency($totalGrossAllTransactions)</h3>
                <div>Jumlah semua pendapatan di transaksi</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-8 mb-3">
        {{-- Grafik Kota dengan Jumlah Petualangan --}}
        <div class="card shadow">
            <div class="card-body">
                <h5 class="mb-3">Grafik Kota dengan Jumlah Petualangan</h5>
                <div class="chart-bar">
                    <canvas id="myBarChartExperience"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('dashboard-admin-scripts')
<script defer>
    let labelData = @json($namaJenis);
    let nilaiData = @json($totalJenis);

    let kotaTerurutBerdasarkanTransaksi = @json($locationByTransaction);
    let xAxisKota = kotaTerurutBerdasarkanTransaksi.map(item => item.nama_location);
    let yAxisKota = kotaTerurutBerdasarkanTransaksi.map(item => item.n_transaksi);

    let kotaTerurutBerdasarkanPetualangan = @json($locationByExperience);
    let xAxisExperience = kotaTerurutBerdasarkanPetualangan.map(item => item.nama_location);
    let yAxisExperience = kotaTerurutBerdasarkanPetualangan.map(item => item.n_experiences);
</script>
@endsection
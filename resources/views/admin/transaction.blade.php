@extends('layouts.dashboard')

@section('content')

    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-3">
                <div class="card border-left-primary">
                    <div class="card-body">
                        <strong>Proyeksi Keuntungan</strong>
                        <h3><strong>@currency($nilai_transaksi)</strong></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-left-success">
                    <div class="card-body">
                        <strong>Nilai Transaksi Berhasil</strong>
                        <h3><strong>@currency( $nilai_berhasil )</strong></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-left-success">
                    <div class="card-body">
                        <strong>Jumlah Transaksi Berhasil</strong>
                        <h3><strong>{{ $success_payments->count() }}</strong></h3>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card border-left-primary">
                    <div class="card-body">
                        <strong>Jumlah Seluruh Transaksi</strong>
                        <h3><strong>{{ $transactions->count() }}</strong></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <h3><strong>Kelola Transaksi Petualangan</strong></h3>
        </div>
        <div class="row my-3">
            <div class="card" style="min-width:100%">
                <div class="card-header">
                    Pending Transaction
                </div>
                <div class="card-body text-center" style="overflow:auto;max-height:500px">
                    @if ( $pendings->count() < 1)
                        <strong>Whoops! It seems that no transaction is available</strong>
                    @else
                        <table class="table">
                            <thead>
                                <th>ID Transaksi</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Host</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($pendings as $pdg)
                                    <tr>
                                        <td>{{ $pdg->transaction_id }}</td>
                                        <td>{{ $pdg->experience->nama_experience }}</td>
                                        <td>{{ $pdg->guest->name }}</td>
                                        <td>{{ $pdg->host->nama_host }}</td>
                                        <td>{{ $pdg->amount }}</td>
                                        <td>{{ $pdg->book_date }}</td>
                                        <td>@currency($pdg->gross_total)</td>
                                        <td>
                                            <form action="/admin/transaction/{{ $pdg->transaction_id }}/update" method="POST">
                                                @csrf
                                                <input type="hidden" name="tr_id" value="{{ $pdg->transaction_id }}">
                                                <select name="status" id="status" onchange="this.form.submit()" class="form-control">
                                                    <option value="{{ $pdg->status }}">{{ $pdg->status }}</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="Success">Success</option>
                                                    <option value="failed">Failed</option>
                                                    <option value="expired">Expired</option>
                                                </select>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="card" style="min-width:100%">
                <div class="card-header bg-success text-white">
                    Success Transaction
                </div>
                <div class="card-body text-center" style="overflow:auto;max-height:500px;">
                    @if ( $success_payments->count() < 1)
                        <strong>Whoops! It seems that no transaction is available</strong>
                    @else
                        <table class="table">
                            <thead>
                                <th>ID Transaksi</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Host</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ( $success_payments as $sc)
                                <tr>
                                    <td>{{ $sc->transaction_id }}</td>
                                    <td>{{ $sc->experience->nama_experience }}</td>
                                    <td>{{ $sc->guest->name }}</td>
                                    <td>{{ $sc->host->nama_host }}</td>
                                    <td>{{ $sc->amount }}</td>
                                    <td>{{ $sc->book_date }}</td>
                                    <td>@currency($sc->gross_total)</td>
                                    <td><button class="btn btn-success btn-sm btn-block">Success</button></td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="card" style="min-width:100%">
                <div class="card-header bg-warning text-white">
                    <strong>Expired Payments</strong>
                </div>
                <div class="card-body text-center" style="overflow:auto;max-height:500px;">
                    @if ( $expireds->count() < 1)
                        <strong>Whoops! It seems that no transaction is available</strong>
                    @else
                        <table class="table">
                            <thead>
                                <th>ID Transaksi</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Host</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ( $expireds as $exp)
                                <tr>
                                    <td>{{ $exp->transaction_id }}</td>
                                    <td>{{ $exp->experience->nama_experience }}</td>
                                    <td>{{ $exp->guest->name }}</td>
                                    <td>{{ $exp->host->nama_host }}</td>
                                    <td>{{ $exp->amount }}</td>
                                    <td>{{ $exp->book_date }}</td>
                                    <td>@currency($exp->gross_total)</td>
                                    <td><button class="btn btn-warning btn-sm btn-block">Expired</button></td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
        <div class="row my-3">
                <div class="card" style="min-width:100%">
                    <div class="card-header bg-danger text-white">
                        <strong>Failed Payments</strong>
                    </div>
                    <div class="card-body text-center" style="overflow:auto;max-height:500px;">
                        @if ( $faileds->count() < 1)
                            <strong>Whoops! It seems that no transaction is available</strong>
                        @else
                            <table class="table">
                                <thead>
                                    <th>ID Transaksi</th>
                                    <th>Experience</th>
                                    <th>Guest</th>
                                    <th>Host</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Value</th>
                                    <th>Status</th>
                                </thead>
                                <tbody>
                                    @foreach ( $faileds as $fail)
                                    <tr>
                                        <td>{{ $fail->transaction_id }}</td>
                                        <td>{{ $fail->experience->nama_experience }}</td>
                                        <td>{{ $fail->guest->name }}</td>
                                        <td>{{ $fail->host->nama_host }}</td>
                                        <td>{{ $fail->amount }}</td>
                                        <td>{{ $fail->book_date }}</td>
                                        <td>@currency($fail->gross_total)</td>
                                        <td><button class="btn btn-danger btn-sm btn-block">Failed</button></td>
    
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
    </div>

@endsection
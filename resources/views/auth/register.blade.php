@extends('layouts.app')

@section('content')
<div class="container-fluid d-flex justify-content-center align-items-center py-3" style="padding-top:56px; background-color:#dfe6eb; height:100vh;top:0">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card shadow">
                <div class="col-md-12">
                    <div class="card-body">
                        <div class="row my-3">
                            <div class="col-md-12">
                                <h4><b>Miliki Sebuah Akun</b></h4>
                            </div>
                        </div>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name" class="text-twalang">{{ __('Name') }}</label>
                                        <input id="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{{ __('Email') }}</label>
                                        <input id="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password">{{ __('Kata Sandi') }}</label>
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="password-confirm">{{ __('Ulangi Kata Sandi') }}</label>
                                        <input id="password-confirm" type="password" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="no_telepon">{{ __('Nomor Telepon') }}</label>
                                        <input id="no_telepon" type="text"
                                            class="form-control @error('no_telepon') is-invalid @enderror"
                                            name="no_telepon" value="{{ old('no_telepon') }}" required
                                            autocomplete="no_telepon">

                                        @error('no_telepon')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin">{{ __('Jenis kelamin') }}</label>
                                        <select id='jenis_kelamin' name="jenis_kelamin" class="form-control" required>
                                            <option value="laki-laki">Laki-laki</option>
                                            <option value="perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lahir">{{ __('Tanggal lahir') }}</label>
                                        <input id="tanggal_lahir" type="date"
                                            class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                            name="tanggal_lahir" value="{{ old('tanggal_lahir') }}"
                                            autocomplete="tanggal_lahir">
                                    </div>
                                    <div class="form-check">
                                        <input required class="form-check-input" type="checkbox" value="" id="cekCentang">
                                        <label class="form-check-label small" for="defaultCheck1">
                                            Saya telah membaca, memahami, dan menyetujui hal-hal yang tercantum pada <a href="">syarat dan ketentuan</a> pengguna Twalang.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 ">
                                    <button type="submit" class="btn btn-primary float-right">
                                        {{ __('Daftar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
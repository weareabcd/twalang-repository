@extends('layouts.pre_host')

@section('content')

    <div class="container-fluid">

        <div class="row align-items-center justify-content-center" style="height:100vh;background-color:#f7fafc">
            <div class="col-md-6 create-experience-image-heading d-flex align-items-center justify-content-center" style="background-image:url('{{ asset('img/host-create.jpg') }}')">
                <div class="row">
                    <div class="col-md-10">
                    </div>
                </div>
            </div>
            <div class="col-md-6 pre-hosting-caption" style="overflow:auto;max-height:100vh">
                <form action="/experience/create" method="POST">
                    @csrf

                <div class="row my-5 justify-content-center">
                    <div class="col-md-9">
                        <div class="card shadow-sm" style="padding:30px">
                            
                            <div class="card-body">
                                <div class="row my-3">
                                    <div class="col-md-12">
                                        <h2><strong>Tawarkan Sebuah Petualangan Tak Terlupakan</strong></h2>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="namaExperience"> Nama Petualangan</label>
                                    <input name="nama_experience" type="text" class="form-control" id="namaExperience"
                                        placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="deskripsiExperience">Deskripsi</label>
                                    <textarea name="deskripsi" class="form-control" id="deskripsiExperience" rows="3"
                                        placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="maxOrang">Kapasitas Orang</label>
                                    <input name="max_orang" type="number" class="form-control" id="maxOrang">
                                </div>
                                <div class="form-group">
                                    <label for="kategoriExperience">Tema</label>
                                    <select name="id_category" class="form-control" id="kategoriExperience">
                                        @foreach ($categories as $category)
                                        <option value="{{$category->id_category}}">{{$category->nama_category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="lokasiExperience">Lokasi</label>
                                    <select name="id_location" class="form-control" id="lokasiExperience">
                                        @foreach ($locations as $location)
                                            <option value="{{$location->id_location}}">{{$location->nama_location}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="alamatExperience">Alamat Lengkap</label>
                                    <textarea name="address" class="form-control" id="alamatExperience"
                                        rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="timeStart">Mulai Pukul</label>
                                            <input name="time_start" type="time" class="form-control" id="timeStart">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="timeEnd">Hingga</label>
                                            <input name="time_end" type="time" class="form-control" id="timeEnd">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input name="harga" type="number" class="form-control" id="harga">
                                </div>
                                <input hidden type="text" name="id_host" value="{{Auth::user()->host->id_host}}">
                                <button class="btn btn-primary" type="submit">Buat Petualangan!</button>
                                <a href="/" class="btn btn-link float-right"><small>< Kembali</small></a>
                            </div>

                        </div>
                    </div>
                </div>

            </form>     
        </div>

    </div>
    
@endsection
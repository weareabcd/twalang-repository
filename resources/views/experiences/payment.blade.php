@extends('layouts.app')

@section('content')
    <div class="container">
        
            <div class="row justify-content-center align-items-center" style="min-height:100vh">
                <div class="col-md-6">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row my-3">
                                    <div class="col-md-12">
                                        <h5 class="text-center"><strong>Shoping Summary</strong></h5>
                                    </div>
                            </div>
                                
                                    <input type="hidden" name="user_id" value="{{ $customer->id }}">
                                    <input type="hidden" name="experience_id" value="{{ $target->id_experience }}">
                                    <input type="hidden" name="book_date" value="{{ $tanggal }}">
                                    <input type="hidden" name="amount" value="{{ $request->amount }}">
                                    
                                    <table class="table">
                                        <tr>
                                            <td><strong>Customer Name</strong></td>
                                            <td>{{ $customer->name }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Experience Item</strong></td>
                                            <td>{{ $target->nama_experience }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Price</strong></td>
                                            <td>@currency($target->harga)</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Guest</strong></td>
                                            <td>{{ $request->amount }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Date</strong></td>
                                            <td>{{ $tanggal->format('l, d M Y') }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>GRAND TOTAL</strong></td>
                                            <td><strong>@currency($total)</strong></td>
                                        </tr>
                                    </table>
                                    <button id="pay-button" class="btn btn-success btn-block">Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        
    </div>
@endsection

@section('scripts')

<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-tudw2QNdD3nN8Vdz"></script>
    <script type="text/javascript">
        document.getElementById('pay-button').onclick = function(){
            // SnapToken acquired from previous step
            snap.pay('<?= $snapToken?>', {
                // Optional
                onSuccess: function(result){
                    
                },
                // Optional
                onPending: function(result){
                 
                },
                // Optional
                onError: function(result){
                   
                }
            });
        };
    </script>

@endsection
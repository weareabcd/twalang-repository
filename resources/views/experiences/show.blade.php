@extends('layouts.app')

@section('content')

<div class="container-fluid experience-heading d-flex align-items-center">
    <div class="container">
        <div class="row my-3 justify-content-center">
            <div id="carouselExampleControls" class="carousel slide col-12" style="height:380px;" data-ride="carousel">
                <div class="carousel-inner" style="border-radius:10px">
                    @php
                    $i = 0;
                    @endphp
                    @forelse ($experience->experience_photos as $experience_photo)
                    <div class="carousel-item @if($i==0) active @endif">
                        <img class="d-block w-100 asyncImage" data-src="{{$experience_photo->link}}"
                            src="{{asset('/img/loading-image.gif')}}"
                            style="height:380px;object-fit:cover;border-radius:10px">
                    </div>
                    @php
                    $i++;
                    @endphp
                    @empty

                    @endforelse
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-2">
                <div class="py-1 px-3 kategori-alam-liar">
                    <div class="kategori-item"><small>{{$experience->category->nama_category}}</small></div>
                </div>
            </div>
        </div>
        <div class="row my-3 align-items-center experience-heading-title">
            <div class="col-md-6">
                <h1><strong>{{$experience->nama_experience}}</strong></h1>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <small>Durasi</small>
                </div>
                @php
                $start = \Carbon\Carbon::parse($experience->time_start);
                $end = \Carbon\Carbon::parse($experience->time_end);
                $hours = $end->diffInHours($start);
                if($start>$end){
                $hours = 24 - $hours;
                }
                @endphp
                <div class="row">
                    <strong>{{$hours}} Jam</strong>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <small>Peserta</small>
                </div>
                <div class="row">
                    <strong>&#8804; {{$experience->max_orang}} orang</strong>
                </div>
            </div>
        </div>
        <div class="row mb-4 text-white">
            <div class="col-md-12">
                <div class="row">
                    <div class="col">
                        <i class="fas fa-map-pin"></i><span class="ml-2">{{$experience->location->nama_location}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <small>{{$experience->address}}</small>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid experience-detail-harga d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <small>TARIF</small>
            </div>
            <div class="col-md-4">
                <small>RATING</small>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h3>@currency($experience->harga)<small>/Orang</small></h3>
            </div>
            <div class="col-md-5">
                <span class="star-rating star-5 checked">
                    <input type="radio" value="1" disabled @if($avg === 1) checked @endif><i></i>
                    <input type="radio" value="2" disabled @if($avg === 2) checked @endif><i></i>
                    <input type="radio" value="3" disabled @if($avg === 3) checked @endif><i></i>
                    <input type="radio" value="4" disabled @if($avg === 4) checked @endif><i></i>
                    <input type="radio" value="5" disabled @if($avg === 5) checked @endif><i></i>
                </span>
            </div>
            <div class="col-md-4">
                <button @guest onclick="loginModal()" @endguest onclick="chooseDate()"
                    class="btn btn-primary btn-block float-right">
                    Pesan
                </button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-5 justify-content-between experience-show-item">
        <div class="col-md-3">
            <h2><strong>Tentang Petualangan Ini</strong></h2>
        </div>
        <div class="col-md-8">
            <small>{{$experience->deskripsi}}</small>
        </div>
    </div>
</div>
<div class="container">
    <div class="row my-5 justify-content-between experience-show-item">
        <div class="col-md-3">
            <h2><strong>Rangkaian Aktivitas</strong></h2>
        </div>
        <div class="col-md-8">
            <div class="activity-list">
                @forelse($experience->activities->sortBy('time_start') as $activity)
                <div class="activity-item">
                    <div class="activity-time my-2">
                        <small>{{Carbon\Carbon::parse($activity->time_start)->format('H:i')}}</small>
                    </div>
                    <div class="activity-content">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="m-auto activity-show-image"
                                    style="background-image:url({{asset('img/loading-image.gif')}}); background-size:100% 100%; object-fit:cover;">
                                    @if($activity->foto!=null)
                                    <img class="activity-show-image" src="{{$activity->foto}}" style="object-fit:cover">
                                    @else
                                    <div class="activity-show-image d-flex justify-content-center align-items-center"
                                        style="background-color:darkslateblue; color:white">
                                        <strong>Belum ada foto</strong>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-link text-left" style="color:black" data-toggle="modal"
                                    data-target="#modalDetailAktivitas{{$activity->id_activity}}">
                                    <h2><small>{{$activity->nama_activity}}</small></h2>
                                </button>
                                <!-- Modal DETAIL AKTIVITAS-->
                                <div class="modal fade" id="modalDetailAktivitas{{$activity->id_activity}}"
                                    tabindex="-1" role="dialog"
                                    aria-labelledby="modalDetailAktivitas{{$activity->id_activity}}Title"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="modalDetailAktivitas{{$activity->id_activity}}Title">
                                                    {{$activity->nama_activity}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {{$activity->deskripsi}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <small>Rangkaian aktivitas belum tercantum.</small>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>
    <div class="row my-5 justify-content-between experience-show-item">
        <div class="col-md-3">
            <h2><strong>Pemandu</strong></h2>
        </div>
        <div class="col-md-8">
            <div class="row align-items-center">
                <div class="col-md-3">
                    <div class="host-show-image">

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><strong>{{$experience->host->nama_host}}</strong></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-outline-primary">
                                Hubungi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5 justify-content-between experience-show-item">
        <div class="col-md-3">
            <h2><strong>Ulasan Pembeli</strong></h2>
        </div>
        <div class="col-md-8">
            @foreach ($experience->transactions as $transaction)
            @if (!is_null($transaction->review_rating))
            <div class="row">
                <div class="col-md-12">
                    <div class="comment">
                        <div class="comment-item">
                            <div class="comment-sender my-2">
                                <strong>{{$transaction->guest->name}}</strong>
                            </div>
                            <div class="comment-content my-2">
                                <span class="star-rating star-5 checked">
                                    <input type="radio" value="1" disabled {{$transaction->review_rating == 1 ? 'checked' : ''}}><i></i>
                                    <input type="radio" value="2" disabled {{$transaction->review_rating == 2 ? 'checked' : ''}}><i></i>
                                    <input type="radio" value="3" disabled {{$transaction->review_rating == 3 ? 'checked' : ''}}><i></i>
                                    <input type="radio" value="4" disabled {{$transaction->review_rating == 4 ? 'checked' : ''}}><i></i>
                                    <input type="radio" value="5" disabled {{$transaction->review_rating == 5 ? 'checked' : ''}}><i></i>
                                </span>
                            </div>
                            <div class="comment-content my-2">
                                {{$transaction->review_description}}
                            </div>
                            <div class="col-md-12">
                                <div class="comment-time text-right">
                                    <small>November 2019</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="container-fluid similar-experience d-flex align-items-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><strong>Petualangan Serupa</strong></h2>
            </div>
        </div>
        <div class="row my-5">
            @php
            $i = 0;
            //Biar random
            $similars_iterator = iterator_to_array($similars);
            shuffle($similars_iterator);
            @endphp
            @foreach ($similars_iterator as $similar)
            @if($i < 4) <div class="col-md-3 my-2">
                <a href="/experiences/show/{{$similar->id_experience}}" target="_blank"
                    class="card card-experience petualangan" style="text-decoration:none; color:#363a40;">
                    <img data-src="{{$similar->experience_photos->first()['link']}}"
                        src="{{asset('img/loading-image.gif')}}" class="card-img-top asyncImage"
                        style="height:220px;object-fit: cover;">
                    <div class="card-footer-twalang" style="min-height:150px;">
                        <strong>{{$similar->nama_experience}}</strong><br>
                        <i class="fas fa-map-pin"></i><span
                            class="ml-2"><small>{{$experience->location->nama_location}}</small></span><br>
                        @php
                        if(strlen($similar->deskripsi)<=60){ $deskripsi_singkat=$similar->deskripsi;
                            }else{
                            $deskripsi_singkat = substr($similar->deskripsi, 0, 60);
                            $deskripsi_singkat = $similar->deskripsi."...";
                            }
                            @endphp
                            <small>{{$deskripsi_singkat}}</small>
                    </div>
                </a>
        </div>
        @endif
        @php
        $i++;
        @endphp
        @endforeach
    </div>

</div>
</div>
<div class="container-fluid" style="min-height:250px; background-color:#1C519B;">
    <div class="container">
        <div class="row" style="padding-top:100px">
            <div class="col-md-4 text-center">
                <h4 class="navbar-brand text-white">
                    Twalang
                </h4>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Privasi & Kebijakan</a>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Layanan Pelanggan</a>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Pertanyaan Umum</a>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Hi! Twalang</a>
            </div>
        </div>
    </div>
</div>


<!--  CHOOSE DATE MODAL -->
<div class="modal fade" id="chooseDateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="/experience/payment" method="post">
                <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                <input type="hidden" name="experience_id" value="{{ $experience->id_experience }}">
                @csrf
                <div class="modal-body" style="padding:30px">

                    <div class="row my-3 justify-content-center align-items-center">
                        <div class="col-md-10">
                            <h4><strong>{{ $experience->nama_experience }}</strong></h4>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-center align-items-center">
                        <div class="col-md-10">
                            <h4><strong>@currency( $experience->harga )</strong><small>/Orang</small></h4>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-center">
                        <div class="col-md-5">
                            <label for="tanggal">Tanggal</label>
                            <input type="date" name="tanggal" id="tanggal" class="form-control"
                                value="{{ $defaultInput }}" required>
                        </div>
                        <div class="col-md-5">
                            <label for="amount">Jumlah Peserta</label>
                            <input type="number" name="amount" onchange="changeSubTotal()" id="amount"
                                class="form-control" value="1" required>
                        </div>
                    </div>

                    <div class="row my-3 justify-content-center">
                        <div class="col-md-10">
                            <button type="submit" class="btn btn-success btn-block">Pesan Sekarang</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

@section('scripts')

<script>


</script>

@endsection
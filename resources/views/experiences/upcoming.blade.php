@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:75px">
    <div class="row my-3">
        <div class="col-md-12">
            <h3>Petualangan Saya</h3>
        </div>
    </div>

    @if ( $pendings->count() > 0)
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h5><strong>Petualangan Belum Dibayar</strong></h5>
                    @foreach ($pendings as $pdn)
                    <div class="card my-3">
                        <div class="card-body" style="overflow:auto;max-height:500px">
                            <div class="row text-center align-items-center">
                                <div class="col-md-4">
                                    <h5>{{ $pdn->experience->nama_experience }}</h5>
                                </div>
                                <div class="col-md-2">
                                    Peserta
                                    <h4>{{ $pdn->amount }}</h4>
                                </div>
                                <div class="col-md-2">
                                    Harga
                                    <h4>@currency($pdn->gross_total)</h4>
                                </div>
                                <div class="col-md-2">
                                    Tanggal
                                    <h4>{{ $pdn->book_date->format('d M Y') }}</h4>
                                </div>
                                <div class="col-md-2">
                                    <button onclick="pay_button('{{$pdn->snap_token}}')"
                                        class="btn btn-primary btn-block">Pay now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row my-3">
        <div class="col-md-12">
            <div class="card shadow-sm">
                <div class="card-body">
                    <h5><strong>Petualangan Mendatang</strong></h5>
                    @if ($upcomings->count() < 1) <div class="card my-3">
                        <div class="card-body">
                            <div class="col-md-12 text-center">
                                Tidak Ada Petualangan Mendatang
                            </div>
                        </div>
                </div>
                @endif
                @foreach ($upcomings as $pdn)
                <div class="card my-3">
                    <div class="card-body" style="overflow:auto;max-height:500px">
                        <div class="row text-center align-items-center">
                            <div class="col-md-4">
                                <h5>{{ $pdn->experience->nama_experience }}</h5>
                            </div>
                            <div class="col-md-2">
                                Peserta
                                <h4>{{ $pdn->amount }}</h4>
                            </div>
                            <div class="col-md-2">
                                Harga
                                <h4>@currency($pdn->gross_total)</h4>
                            </div>
                            <div class="col-md-4">
                                Tanggal
                                <h4>{{ $pdn->book_date->diffForHumans() }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="row my-3">
    <div class="col-md-12">
        <div class="card shadow-sm">
            <div class="card-body">
                <h5><strong>Petualangan Selesai</strong></h5>
                @if ($past->count() < 1) <div class="card my-3">
                    <div class="card-body">
                        <div class="col-md-12 text-center">
                            Tidak Ada Petualangan Selesai
                        </div>
                    </div>
            </div>
            @endif
            @foreach ($past->sortByDesc('created_at') as $pdn)
            <div class="card my-3">
                <div class="card-body" style="overflow:auto;max-height:500px">
                    <div class="row text-center align-items-center">
                        <div class="col-md-4">
                            <h5>{{ $pdn->experience->nama_experience }}</h5>
                        </div>
                        <div class="col-md-2">
                            Peserta
                            <h4>{{ $pdn->amount }}</h4>
                        </div>
                        <div class="col-md-2">
                            Harga
                            <h4>@currency($pdn->gross_total)</h4>
                        </div>
                        <div class="col-md-4">
                            Tanggal
                            <h4>{{ $pdn->book_date->format('d F Y') }}</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            {{-- jika masih belum ngerating --}}
                            @if (is_null($pdn->review_rating))
                            <div class="row mt-3">
                                <div class="col-12 mb-3">
                                    Bagaimana pengalaman petualangan anda?
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <form action="/rate/{{$pdn->transaction_id}}" method="POST">
                                        @csrf
                                        <div class="form-group text-center">
                                            <span class="star-rating star-5">
                                                <input type="radio" name="review_rating" value="1" required><i></i>
                                                <input type="radio" name="review_rating" value="2"><i></i>
                                                <input type="radio" name="review_rating" value="3"><i></i>
                                                <input type="radio" name="review_rating" value="4"><i></i>
                                                <input type="radio" name="review_rating" value="5"><i></i>
                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="review_description" id="review_description"
                                                class="form-control" placeholder="Tulis review anda di sini..."
                                                rows="5"></textarea>
                                        </div>
                                        <div class="form-group text-right">
                                            <button class="btn btn-primary" type="submit">Kirim Review</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @else
                            <div class="row mt-3">
                                <div class="col-12 mb-3">
                                    Review anda:
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group text-center">
                                        <span class="star-rating star-5 checked">
                                            <input type="radio" value="1" disabled {{$pdn->review_rating == 1 ? 'checked' : ''}}><i></i>
                                            <input type="radio" value="2" disabled {{$pdn->review_rating == 2 ? 'checked' : ''}}><i></i>
                                            <input type="radio" value="3" disabled {{$pdn->review_rating == 3 ? 'checked' : ''}}><i></i>
                                            <input type="radio" value="4" disabled {{$pdn->review_rating == 4 ? 'checked' : ''}}><i></i>
                                            <input type="radio" value="5" disabled {{$pdn->review_rating == 5 ? 'checked' : ''}}><i></i>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <textarea name="review_description" id="review_description"
                                            class="form-control" placeholder="Tulis review anda di sini..." rows="5" disabled>{{$pdn->review_description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-tudw2QNdD3nN8Vdz"></script>
<script type="text/javascript">
    function pay_button(snap_token){
        // SnapToken acquired from previous step
        snap.pay(snap_token, {
            // Optional
            onSuccess: function(result){
                    
            },
            // Optional
            onPending: function(result){
             
            },
            // Optional
            onError: function(result){
               
            }
        });
    };
</script>

@endsection
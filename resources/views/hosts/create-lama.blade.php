@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:56px">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card my-3">
                <div class="card-header">Menjadi Host Baru</div>
                <div class="card-body">
                    {{-- form --}}
                    <form action="/hosts/create" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="namaHost">Nama Host</label>
                            <input type="text" class="form-control" id="namaHost" name = "nama_host" value="{{Auth::user()->name}}">
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="inputFotoHost">Upload Foto Host</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01"
                                    aria-describedby="inputFotoHost">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                        <input hidden type="text" name="id_user" value="{{Auth::user()->id}}">
                        <button class="btn btn-primary" type="submit">Jadi Host!</button>
                        <a href="/" class="btn btn-link">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
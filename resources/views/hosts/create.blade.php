@extends('layouts.pre_host')

@section('content')

    <div class="container-fluid">

        <div class="row align-items-center justify-content-center" style="height:100vh;background-color:#f7fafc">
            <div class="col-md-6 pre-hosting-1-image d-flex" style="background-image:url('{{ asset('img/host-create.jpg') }}')">

            </div>
            <div class="col-md-6 pre-hosting-caption">
                <div class="row my-3 justify-content-center">
                    <div class="col-md-10">
                        <form action="/hosts/create" method="POST">
                            @csrf
                            <div class="card shadow-sm" style="border:none">
                                <div class="card-body" style="padding:50px">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h2><strong>Atur nama kamu sebagai pemandu</strong></h2>
                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-10">
                                            <small>Nama ini akan muncul sebagai tuan rumah pada halaman petualangan yang kamu tawarkan</small>
                                       </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" name="nama_host" id="nama_host" class="form-control" value="{{Auth::user()->name}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <input hidden type="text" name="id_user" value="{{Auth::user()->id}}">
                                    <div class="row my-3">
                                        <div class="col-md-12 d-flex justify-content-between">
                                            <a href="/pre/3"><small>< kembali</small></a>
                                            <button type="submit" class="btn btn-primary">Menjadi Pemandu</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
@endsection
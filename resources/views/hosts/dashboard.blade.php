@extends('layouts.dashboard')

@section('content')
@if (session('status'))
<div class="alert alert-success">
    <strong>{{ session('status') }}</strong>
</div>
@endif
<div class="row mb-3">
    <div class="col-12">
        <h2><strong>Hai! Selamat Datang, {{Auth::user()->host->nama_host}}!</strong></h2>
    </div>
</div>
<div class="row">
    <div class="mb-3 col-lg-8">
        <div class="row">
            <div class="mb-3 col-12 d-flex flex-wrap">
                {{-- untuk pengatur waktu dashboard --}}
                <a href="/dashboard/time/thisweek"
                    class="text-decoration-none card shadow mr-2 @if($time=='thisweek') time-active @endif"
                    style="border-radius: 99px">
                    <div class="py-2 px-3">
                        Minggu ini
                    </div>
                </a>
                <a href="/dashboard/time/thismonth"
                    class="text-decoration-none card shadow mr-2 @if($time=='thismonth') time-active @endif"
                    style="border-radius: 99px">
                    <div class="py-2 px-3">
                        Bulan ini
                    </div>
                </a>
                <a href="/dashboard/time/thisyear"
                    class="text-decoration-none card shadow mr-2 @if($time=='thisyear') time-active @endif"
                    style="border-radius: 99px">
                    <div class="py-2 px-3">
                        Tahun ini
                    </div>
                </a>
                <a href="/dashboard/time/all"
                    class="text-decoration-none card shadow mr-2 @if($time=='all') time-active @endif"
                    style="border-radius: 99px">
                    <div class="py-2 px-3">
                        Semua data
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="mb-3 col-sm-4">
                {{-- untuk pemesanan selesai --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0">{{$nDoneTransactions}}</h3>
                        <div>Selesai</div>
                    </div>
                </div>
            </div>
            <div class="mb-3 col-md-4">
                {{-- untuk pemesanan mendatang --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0">{{$nSoonTransactions}}</h3>
                        <div>Mendatang</div>
                    </div>
                </div>
            </div>
            <div class="mb-3 col-md-4">
                {{-- untuk pemesanan belum dibayar --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0">{{$nNotPaid}}</h3>
                        <div>Belum Bayar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="mb-3 col-12">
                {{-- grafik line chart --}}
                <div class="card shadow">
                    <div class="card-body">
                        <div class="chart-area">
                            <canvas id="myAreaChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="mb-3 col-sm-4">
                {{-- untuk pemesanan selesai --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0">@currency($income)</h3>
                        <div>Pendapatan</div>
                    </div>
                </div>
            </div>
            <div class="mb-3 col-md-4">
                {{-- untuk pemesanan mendatang --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0">@currency($willIncome)</h3>
                        <div>Proyeksi Pendapatan</div>
                    </div>
                </div>
            </div>
            <div class="mb-3 col-md-4">
                {{-- untuk pemesanan belum dibayar --}}
                <div class="card shadow">
                    <div class="card-body text-center">
                        <h3 class="mb-0 text-truncate" style="color: @if($diffPercent>=0)#1ee652 @else #cf1738 @endif"><i class="fas @if($diffPercent>=0) fa-angle-double-up @else fa-angle-double-down @endif"></i> {{$diffPercent}}%</h3>
                        <div>Perbandingan</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h3 class="mb-3"><strong>Tema petualangan yang sering dipesan</strong></h3>
                <div class="row overflow-auto">
                    <div class="col-12 d-flex flex-wrap">
                        @foreach ($categoryByTransaction as $ct)
                        <div class="card shadow mr-3 mb-3">
                            <div class="card-body">
                                <strong>#{{$loop->iteration}} {{$ct->nama_category}}</strong>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-3 col-lg-4">
        {{-- untuk todolist --}}
        <div class="card shadow">
            <div class="card-body">
                <strong>Petualangan Mendatang</strong>
                @foreach ($upcomingExperiences as $ue)
                    <div class="card mt-3">
                        <div class="card-body" @if($ue->book_date == Carbon\Carbon::today()) style="background-color:#1079D3; color:white" @endif>
                            <div>{{$ue->experience->nama_experience}}</div>
                            <div><small>{{$ue->book_date == Carbon\Carbon::today() ? 'Hari ini' : $ue->book_date->diffForHumans()}}</small></div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        {{-- ulasan --}}
        <h3 class="my-3"><strong>Ulasan Tamu</strong></h3>
        @foreach (Auth::user()->host->experiences as $experience)
        <div class="card shadow">
            <div class="card-body">
                <strong>{{$experience->nama_experience}}</strong>
                <hr>
                <div class="overflow-auto" style="max-height: 300px">
                    @foreach ($experience->transactions->sortByDesc('created_at') as $tr)
                    @if (!is_null($tr->review_rating))
                    <h6><strong>{{$tr->guest->name}}</strong></h6>
                    <span class="star-rating star-5 checked">
                        <input type="radio" value="1" disabled {{$tr->review_rating == 1 ? 'checked' : ''}}><i></i>
                        <input type="radio" value="2" disabled {{$tr->review_rating == 2 ? 'checked' : ''}}><i></i>
                        <input type="radio" value="3" disabled {{$tr->review_rating == 3 ? 'checked' : ''}}><i></i>
                        <input type="radio" value="4" disabled {{$tr->review_rating == 4 ? 'checked' : ''}}><i></i>
                        <input type="radio" value="5" disabled {{$tr->review_rating == 5 ? 'checked' : ''}}><i></i>
                    </span>
                    <p>{{$tr->review_description}}</p>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('dashboard-scripts')
    <script defer>
        let xAxis = @json($xAxis);
        let yAxis = @json($yAxis);
    </script>
@endsection
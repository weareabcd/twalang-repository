@extends('layouts.dashboard')

@section('content')
<div class="row">
    @forelse (Auth::user()->host->experiences as $experience)
    <div class="col-md-12">
        <a href="/kelola_petualangan/show/{{$experience->id_experience}}" style="text-decoration:none; color:#363a40">
            <div class="card shadow-sm card-experience border-bottom-primary mb-4">
                <div class="card-body">
                    <h3 class="mb-3"><strong>{{$experience->nama_experience}}</strong></h3>
                    <table class="sub-title-2">
                        <tr>
                            <td><i class="fas fa-tag mr-2"></i></td>
                            <td>{{$experience->category->nama_category}}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-map-marker-alt mr-2"></i></td>
                            <td>{{$experience->location->nama_location}}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-clock mr-2"></i></td>
                            @php
                            $start = \Carbon\Carbon::parse($experience->time_start);
                            $end = \Carbon\Carbon::parse($experience->time_end);
                            $hours = $end->diffInHours($start);
                            if($start>$end){
                                $hours =  24 - $hours;
                            }   
                            @endphp
                            <td>{{$hours}} jam</td>
                        </tr>
                    </table>
                    <h4 style="float:right"><strong>@currency($experience->harga)</strong></h4>
                </div>
            </div>
        </a>
    </div>
    @empty
    <div style="padding-top:80px;text-align:center;width:100%">
        <img class="mb-3" style="width:300px;" src="{{ asset('img/ilustrasi-kosong.png') }}" alt="">
        <h5><strong>Anda belum menambahkan petualangan.</strong> </h5>
    </div>
    @endforelse
</div>
@endsection
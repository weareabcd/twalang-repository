@extends('layouts.dashboard')

@section('content')
@if (session('status'))
<div class="alert alert-success alert-message" id="alert-message">
    <strong>{{ session('status') }}</strong>
</div>
@endif

<nav aria-label="breadcrumb">
    <ol class="breadcrumb d-flex align-items-center">
        <li class="breadcrumb-item"><a href="/kelola_petualangan">Kelola Petualangan</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$experience->nama_experience}}</li>
        <li class="ml-auto"><a href="/experiences/show/{{$experience->id_experience}}" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i> <strong>sisi pengguna</strong></a></li>
    </ol>
</nav>

<div class="row">
    <div class="col-lg-8">
        <div class="card mb-4">
            <form action="/kelola_petualangan/edit/{{$experience->id_experience}}" method="POST">
                @csrf
                <div class="card-header d-flex justify-content-between">
                    <strong>Detail Petualangan</strong><button type="submit"
                        class="btn btn-primary btn-sm"><strong>SIMPAN</strong></button>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="judulPetualangan"><small>Judul Petualangan</small></label>
                        <input name="nama_experience" type="text" class="form-control" id="judulPetualangan"
                            value="{{$experience->nama_experience}}">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi"><small>Tentang Petualangan</small></label>
                        <textarea name="deskripsi" type="text" rows='4' class="form-control"
                            id="deskripsi">{{$experience->deskripsi}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="address"><small>Alamat Lengkap</small></label>
                        <textarea name="address" type="text" rows='3' class="form-control"
                            id="address">{{$experience->address}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="location"><small>Kota</small></label>
                        <select name="id_location" class="form-control" id="location">
                            @foreach ($locations as $location)
                            <option @if($location->id_location == $experience->id_location) selected @else @endif
                                value="{{$location->id_location}}">{{$location->nama_location}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="card mb-4">
            <form action="/kelola_petualangan/edit/{{$experience->id_experience}}" method="POST">
                @csrf
                <div class="card-header d-flex justify-content-between">
                    <strong>Estimasi Waktu</strong><button type="submit"
                        class="btn btn-primary btn-sm"><strong>SIMPAN</strong></button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="timeStart"><small>Waktu Mulai</small></label>
                                <input name= "time_start" type="time" class="form-control" id="timeStart"
                                    value="{{$experience->time_start}}">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="timeEnd"><small>Waktu Selesai</small></label>
                                <input name = "time_end" type="time" class="form-control" id="timeEnd" value="{{$experience->time_end}}">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between">
                <strong>Aktivitas</strong>
            </div>
            <div class="card-body">
                @foreach ($experience->activities->sortBy('time_start') as $activity)
                <div class="card d-flex col-12 mb-2">
                    <div class="card-body">
                        <div class="row" >
                            <div class="col-3 px-0">
                                <div class="card" >
                                    @if($activity->foto != null)
                                    <img data-src="{{$activity->foto}}" src="{{asset('/img/loading-image.gif')}}" class="card-img asyncImage" style="object-fit:cover; height:90px" >
                                    @else
                                    <div class = "d-flex align-items-center justify-content-center" style="height:90px;background-color:darkslateblue; color:white">
                                        <small>Belum ada foto</small>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-9 ">
                                <strong>{{$activity->nama_activity}}</strong>
                                <div class="row">
                                    <div class="col-12">
                                        <small>Jam {{Carbon\Carbon::parse($activity->time_start)->format('H:i')}}</small>
                                    </div>
                                </div>
                                <div class="float-right btn-group" role="group" style="position:absolute; top:0 ;right:0;">
                                    <button class="btn btn-outline-danger " data-toggle="modal" data-target="#modalDeleteActivity{{$activity->id_activity}}"><i class="fas fa-trash"></i></button>
                                    <button class="btn btn-primary" data-toggle="modal"
                                        data-target="#modalEditActivity{{$activity->id_activity}}"><i
                                            class="fas fa-edit"></i></button>
                                </div>
                                
                            </div>
                            <!-- Modal DELETE-->
                            <div class="modal fade" id="modalDeleteActivity{{$activity->id_activity}}" tabindex="-1" role="dialog" aria-labelledby="modalDeleteActivity{{$activity->id_activity}}Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="modalDeleteActivity{{$activity->id_activity}}Title">Anda Yakin?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    Anda yakin ingin menghapus aktivitas ini?
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <form action="/kelola_petualangan/show/{{$experience->id_experience}}/activity/{{$activity->id_activity}}/delete" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!-- Modal EDIT-->
                            <div class="modal fade" id="modalEditActivity{{$activity->id_activity}}" tabindex="-1"
                                role="dialog" aria-labelledby="modalEditActivity{{$activity->id_activity}}Title"
                                aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <form action="/kelola_petualangan/show/{{$experience->id_experience}}/activity/{{$activity->id_activity}}/edit" method="POST" enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="modalEditActivity{{$activity->id_activity}}Title">
                                                    {{$activity->nama_activity}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="namaActivity">Nama Aktivitas</label>
                                                    <input name="nama_activity" type="text" class="form-control"
                                                        id="namaActivity" value="{{$activity->nama_activity}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="deskripsiActivity">Deskripsi</label>
                                                    <textarea name="deskripsi" class="form-control"
                                                        id="deskripsiActivity"
                                                        rows="3">{{$activity->deskripsi}}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="waktuMulaiActivity1">Waktu Mulai</label>
                                                    <input name="time_start" type="time" class="form-control"
                                                        id="waktuMulaiActivity1" value="{{$activity->time_start}}">
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="custom-file">
                                                        <input onchange="readURLEditAktivitas(this, {{$activity->id_activity}});" type="file" class="custom-file-input" id="editFotoActivity" aria-describedby="editFotoActivityon03" name="thing">
                                                        <label class="custom-file-label" for="editFotoActivity">Pilih Foto Aktivitas</label>
                                                    </div>
                                                </div>
                                                <img id="{{$activity->id_activity}}" src="{{$activity->foto}}" class="col-12 mb-3"/>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Kembali</button>
                                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <button class="btn btn-primary col-12 " data-toggle="modal" data-target="#modalTambahActivity">
                    <strong>Tambah Aktivitas Baru</strong>
                </button>
                <!-- Modal TAMBAH AKTIVITAS-->
                <div class="modal fade" id="modalTambahActivity" tabindex="-1" role="dialog"
                    aria-labelledby="modalTambahActivityTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalTambahActivityTitle">Tambah Aktivitas</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/kelola_petualangan/show/{{$experience->id_experience}}/activity/create"
                                method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="judulAktivitas">Nama Aktivitas</label>
                                        <input name="nama_activity" type="text" class="form-control"
                                            id="judulAktivitas">
                                    </div>
                                    <div class="form-group">
                                        <label for="deskripsi"><small>Deskripsi</small></label>
                                        <textarea name="deskripsi" type="text" rows='3' class="form-control"
                                            id="deskripsi"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="waktuMulaiActivity">Waktu Mulai</label>
                                        <input name="time_start" type="time" class="form-control"
                                            id="waktuMulaiActivity">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="custom-file">
                                            <input onchange="readURL2(this);" type="file" class="custom-file-input" id="inputFotoActivity" aria-describedby="inputFotoActivityon03" name="thing">
                                            <label class="custom-file-label" for="inputFotoActivity">Pilih Foto Aktivitas</label>
                                        </div>
                                    </div>
                                    <img id="foto-aktivitas" src="" class="col-12 mb-3"/>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Kembali</button>
                                    <button type="submit" class="btn btn-primary">Tambah Aktivitas</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- FOTO EXPERIENCE --}}
    <div class="col-lg-4">
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between">
                <strong>Foto Experience</strong>
            </div>
            <div class="card-body" style="max-height: 380px; overflow-y: auto;">
                <form action="/kelola_petualangan/{{$experience->id_experience}}/experience_photos/create" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input required onchange="readURL(this);" type="file" class="custom-file-input" id="inputGroupFile03" aria-describedby="inputGroupFileAddon03" name="thing">
                            <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                        </div>
                    </div>
                    <img id="foto-experience" src="" class="col-12 mb-3"/>
                    <button class="col-12 btn btn-primary" type="submit">Upload Foto</button>
                </form>
                <hr class="d-none d-md-block">
                <div class="row mb-2">
                    <div class="col">
                        <strong>Foto Petualanganmu</strong>
                    </div>
                </div>
                <div class="row">
                    @forelse($experience->experience_photos as $experience_photo)
                    <div class="col-12 mb-2">
                        <div class="card foto">
                            <button type="submit" class="hapus-foto btn btn-danger mb-1 mr-1" style="position:absolute;bottom:0; right:0; z-index:1" data-toggle="modal" data-target="#modalHapusFoto{{$experience_photo->id}}"><i class="fas fa-trash fa-sm"></i></button>
                            <img  class="card-img asyncImage" data-src="{{$experience_photo->link}}" src= "{{asset('img/loading-image.gif')}}"   style="object-fit: cover;">
                            {{-- MODAL HAPUS FOTO --}}
                            <div class="modal fade" id="modalHapusFoto{{$experience_photo->id}}" tabindex="-1" role="dialog" aria-labelledby="modalHapusFoto{{$experience_photo->id}}Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="modalHapusFoto{{$experience_photo->id}}Title">Anda Yakin?</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    Anda yakin ingin menghapus foto?
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                    <form action="/kelola_petualangan/{{$experience->id_experience}}/experience_photos/{{$experience_photo->id}}/delete" method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="col-12 d-flex justify-content-center">
                        Belum ada foto
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <form action="/kelola_petualangan/edit/{{$experience->id_experience}}" method="POST">
                @csrf
                <div class="card-header d-flex justify-content-between">
                    <strong>Tarif Petualangan</strong><button type="submit"
                        class="btn btn-primary btn-sm"><strong>SIMPAN</strong></button>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="harga"><small>Tarif Petualangan</small></label>
                        <input name = "harga" type="number" class="form-control" id="harga" value="{{$experience->harga}}">
                    </div>
                </div>
            </form>
        </div>
        <div class="card mb-4">
            <form action="/kelola_petualangan/edit/{{$experience->id_experience}}" method="POST">
                @csrf
                <div class="card-header d-flex justify-content-between">
                    <strong>Tema Petualangan</strong><button type="submit"
                        class="btn btn-primary btn-sm"><strong>SIMPAN</strong></button>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="category"><small>Pilih Tema</small></label>
                        <select name = "id_category" class="form-control" id="location">
                            @foreach ($categories as $category)
                            <option @if($category->id_category == $experience->id_category) selected @else @endif
                                value="{{$category->id_category}}">{{$category->nama_category}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="card mb-4">
            <form action="/kelola_petualangan/edit/{{$experience->id_experience}}" method="POST">
                @csrf
                <div class="card-header d-flex justify-content-between">
                    <strong>Kapasitas Pengunjung</strong><button type="submit"
                        class="btn btn-primary btn-sm"><strong>SIMPAN</strong></button>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="maxOrang"><small>Tentukan Kapasitas</small></label>
                        <input name = "max_orang" type="number" class="form-control" id="maxOrang" value="{{$experience->max_orang}}">
                    </div>
                </div>
            </form>
        </div>
        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between">
                <strong>Hapus Petualangan</strong>
            </div>
            <div class="card-body">
                <button class="col-12 btn btn-outline-danger">Hapus Petualangan</button>
            </div>
        </div>
    </div>
</div>
@endsection
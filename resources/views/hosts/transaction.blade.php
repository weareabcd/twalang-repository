@extends('layouts.dashboard')

@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col-md-4">
                <div class="card shadow-sm border-left-primary">
                    <div class="card-body text-center">
                        <h4><small>Proyeksi Keuntungan</small></h4>
                        <strong>@currency($proyeksi_keuntungan)</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-sm border-left-success">
                    <div class="card-body text-center">
                        <h4><small>Total Keuntungan</small></h4>
                        <strong>@currency($total_keuntungan)</strong>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card shadow-sm border-left-success">
                    <div class="card-body text-center">
                        <h4><small>Jumlah Tamu Terlayani</small></h4>
                        <strong>{{$tamu_terlayani}} Orang</strong>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <h3>Pemesanan Petualanganku</h3>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($pendings as $tr)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $tr->experience->nama_experience }}</td>
                                        <td>{{ $tr->guest->name }}</td>
                                        <td>{{ $tr->amount }}</td>
                                        <td>{{ $tr->book_date }}</td>
                                        <td>@currency($tr->gross_total)</td>
                                        <td>{{ $tr->status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <h3>Petualangan Mendatang</h3>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($upcomings as $upc)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $upc->experience->nama_experience }}</td>
                                        <td>{{ $upc->guest->name }}</td>
                                        <td>{{ $upc->amount }}</td>
                                        <td>{{ $upc->book_date->diffForHumans() }}</td>
                                        <td>@currency($upc->gross_total)</td>
                                        <td><button class="btn btn-success btn-block">{{ $upc->status }}</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <h3>Petualangan Selesai</h3>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>No</th>
                                <th>Experience</th>
                                <th>Guest</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th>Value</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach ($completeds as $upc)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $upc->experience->nama_experience }}</td>
                                        <td>{{ $upc->guest->name }}</td>
                                        <td>{{ $upc->amount }}</td>
                                        <td>{{ $upc->book_date->format('d F Y') }}</td>
                                        <td>@currency($upc->gross_total)</td>
                                        <td><button class="btn btn-success btn-block">{{ $upc->status }}</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-twalang shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler mb-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                {{-- Bisa walaupun keduanya --}}
                @if(Request::is('/'))
                @else
                <li class="nav-item mr-3">
                    {{-- <form class="navbar-search" style="width:100%"> --}}
                    <form id="formSearch" name="formSearch" action="/search/result" class="form-inline">
                        <div class="input-group">
                            <input name="keyword" form="formSearch" type="text" class="form-control bg-light border-0 small" placeholder="Cari petualangan..."
                                aria-label="Search" aria-describedby="basic-addon2" @if(Request::segment(1) === 'search') value="{{$target['keyword']}}" @endif >
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit" form="formSearch">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                        @if(Request::segment(1) != 'search')
                        <input type="text" name="kota" hidden>
                        <input type="text" name="jenis_petualangan" hidden>
                        @endif
                    </form>
                </li>
                @endif
            </ul>
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a @guest href="#" onclick="loginModal()" @else @if(Auth::user()->role=='host')
                        href="/experiences/create"
                        @else
                        href="/pre/1"
                        @endif
                        @endguest class="nav-link"><strong>{{ __('Buat Petualangan') }}</strong></a>
                </li>
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="loginModal()">{{ __('Masuk') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Daftar') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    {{-- dropdown --}}
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                        aria-labelledby="navbarDropdown">
                        {{-- Dropdown untuk kelola host --}}
                        @if(Auth::user()->role=='host')        
                        <a class="dropdown-item" href="/dashboard" onclick="">
                            {{ __('Dashboard') }}
                        </a>
                        @elseif(Auth::user()->role=='admin')
                        <a class="dropdown-item" href="/dashboard/admin" onclick="">
                            {{ __('Dashboard') }}
                        </a>
                        @else
                        <a class="dropdown-item" href="/user/experience" onclick="">
                            {{ __('Petualanganku') }}
                        </a>
                        @endif
                        {{-- Dropdown untuk logout --}}
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
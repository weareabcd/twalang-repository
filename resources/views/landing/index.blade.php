@extends('layouts.app')

@section('content')

<div class="home-galery container-fluid d-flex align-items-center" style="background-image: url(' {{ asset('img/bg-satu.jpg')}} '); margin-top:56px">
    <div class="container px-0" style="visibility: visible;">
        <div class="row ">
            <div class="col-lg-6 d-flex align-items-center" >
                <h1 class="text-white my-4"><strong>Rayakan kebebasan <br>dengan petualangan</strong></h1>
            </div>
            <div class="col-lg-6 d-flex justify-content-end">
                <div class="card shadow-lg">
                    <div class="card-body">
                        <h4 class="pt-3 px-3"><strong>Temukan petualangan yang <br> tak terlupakan</strong></h4>
                        <form action="/search/result" class="py-3 px-3" id="formSearch">
                            <div class="form-group">
                                <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Coba ketik sesuatu">
                            </div>
                            
                            <div class="form-group">
                                <label for="kota">Kota</label>
                                <select name="kota" id="kota" class="form-control">
                                        <option value="">Semua kota</option>
                                    @foreach ( $locations as $loc)
                                        <option value="{{ $loc->id_location }}">{{ $loc->nama_location }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="jenis_petualangan">Jenis Petualangan</label>
                                <select name="jenis_petualangan" id="jenis_petualangan" class="form-control">
                                        <option value="">Semua Kategori</option>
                                    @foreach ( $categories as $cat)
                                        <option value="{{ $cat->id_category }}">{{ $cat->nama_category }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">temukan!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="container">
    <div class="row my-4">
        <div class="col-md-12">
            <h3 class="sub-title-3"><strong>Lokasi Populer</strong></h3>
        </div>
    </div>
    <div class="scrolling-wrapper mb-4">
        @foreach ($locations as $location)
            <a href="#0" onclick="cariSubmit1({{$location->id_location}})" class="card card-experience card-scrollable my-2 mx-2" style="width:230px;text-decoration:none; color:#363a40;">
                <img class="card-img-top" src="{{asset("/img/locations/$location->foto")}}" style="height:20vw;object-fit:cover">
                <div class="card-footer-twalang">
                    <strong>{{$location->nama_location}}</strong>
                </div>
            </a>
        @endforeach
    </div>
    <div class="row my-4">
        <div class="col-md-12">
            <h3 class="sub-title-3"><strong>Petualangan yang mungkin kamu suka</strong></h3>
        </div>
    </div>
    <div class="row my-2">
        <div class="col-md-3">
            <ul class="list-group">
                <li class="list-group-item-twalang list-group-item-action my-1"><strong>Semua</strong></li>
                @foreach ($categories as $category)
                    <li class="list-group-item-twalang list-group-item-action my-1">{{$category->nama_category}}</li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-9">
            <div class="row">
                @php
                    $i = 0;
                    //Biar random
                    $experiences_iterator = iterator_to_array($experiences);
                    shuffle($experiences_iterator);
                @endphp
                @foreach ($experiences_iterator as $experience)
                    @if($i < 9)
                        <div class="col-md-4 my-2">
                            <a href="/experiences/show/{{$experience->id_experience}}" target="_blank" class="card card-experience petualangan" style="text-decoration:none; color:#363a40;">
                                <img data-src="{{$experience->experience_photos->first()['link']}}" src="{{asset('img/loading-image.gif')}}" class="card-img-top asyncImage" style="height:220px;object-fit: cover;">
                                <div class="card-footer-twalang" style="min-height:150px;">
                                    <strong>{{$experience->nama_experience}}</strong><br>                        
                                    <i class="fas fa-map-pin"></i><span class = "ml-2"><small>{{$experience->location->nama_location}}</small></span><br>
                                    @php
                                        if(strlen($experience->deskripsi)<=60){
                                            $deskripsi_singkat = $experience->deskripsi;
                                        }else{
                                            $deskripsi_singkat = substr($experience->deskripsi, 0, 60);
                                            $deskripsi_singkat = $deskripsi_singkat."...";
                                        }
                                    @endphp
                                    <small>{{$deskripsi_singkat}}</small>
                                </div>
                            </a>
                        </div>
                    @endif
                    @php
                        $i++;
                    @endphp
                @endforeach
            </div>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-md-12">
            <a href="#" class="float-right"><small>Lihat semuanya..</small></a>
        </div>
    </div>
    <div class="row my-5">
        <div class="col-md-12">
            <a @guest href="#" onclick="loginModal()" @else @if(Auth::user()->role=='host')
                    href="/experiences/create"
                    @else
                    href="/pre/1"
                    @endif
                    @endguest class="">
                <img class = "card-img card-experience" src="{{ asset('img/banner-1.png') }}" alt="">
            </a>
        </div>
    </div>
</div>

<div class="container-fluid mt-5" style="min-height:250px; background-color:#1C519B;">
    <div class="container">
        <div class="row" style="padding-top:100px">
            <div class="col-md-4 text-center" >
                <h4 class="navbar-brand text-white">
                    Twalang
                </h4>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Privasi & Kebijakan</a>
            </div>
            <div class="col md-2 text-left">
                <a class="link-twalang" href="#">Layanan Pelanggan</a>
            </div><div class="col md-2 text-left">
                <a class="link-twalang" href="#">Pertanyaan Umum</a>
            </div><div class="col md-2 text-left">
                <a class="link-twalang" href="#">Hi! Twalang</a>
            </div>
        </div>
    </div>
</div>

@endsection
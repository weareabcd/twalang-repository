<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dashboard</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Twalang') }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/sb-admin-2.min.css') }}" />
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="{{ URL::asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="{{URL::asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

  <style>
    .star-rating {
      font-size: 0;
      white-space: nowrap;
      display: inline-block;
      /* width: 250px; remove this */
      height: 25px;
      overflow: hidden;
      position: relative;
      background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
      background-size: contain;
    }

    .star-rating i {
      opacity: 0;
      position: absolute;
      left: 0;
      top: 0;
      height: 100%;
      /* width: 20%; remove this */
      z-index: 1;
      background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjRkZERjg4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
      background-size: contain;
    }

    .star-rating input {
      -moz-appearance: none;
      -webkit-appearance: none;
      opacity: 0;
      display: inline-block;
      /* width: 20%; remove this */
      height: 100%;
      margin: 0;
      padding: 0;
      z-index: 2;
      position: relative;
    }

    .star-rating input:hover+i,
    .star-rating input:checked+i {
      opacity: 1;
    }

    .star-rating input:disabled:not(:checked)+i {
      opacity: 0;
    }

    .star-rating i~i {
      width: 40%;
    }

    .star-rating i~i~i {
      width: 60%;
    }

    .star-rating i~i~i~i {
      width: 80%;
    }

    .star-rating i~i~i~i~i {
      width: 100%;
    }

    ::after,
    ::before {
      height: 100%;
      padding: 0;
      margin: 0;
      box-sizing: border-box;
      text-align: center;
      vertical-align: middle;
    }

    .star-rating.star-5 {
      width: 125px;
    }

    .star-rating.star-5 input,
    .star-rating.star-5 i {
      width: 20%;
    }

    .star-rating.star-5 i~i {
      width: 40%;
    }

    .star-rating.star-5 i~i~i {
      width: 60%;
    }

    .star-rating.star-5 i~i~i~i {
      width: 80%;
    }

    .star-rating.star-5 i~i~i~i~i {
      width: 100%;
    }

    .star-rating.star-3 {
      width: 150px;
    }

    .star-rating.star-3 input,
    .star-rating.star-3 i {
      width: 33.33%;
    }

    .star-rating.star-3 i~i {
      width: 66.66%;
    }

    .star-rating.star-3 i~i~i {
      width: 100%;
    }
  </style>
</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion bg-twalang" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/dashboard">
        <div class="sidebar-brand-icon ">
          <i class="fas fa-tachometer-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-3 font-bold">Dashboard</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item {{ Request::segment(1) === 'dashboard' ? 'active' : null }}">
        <a class="nav-link" @if(Auth::user()->role=='admin') href="/dashboard/admin" @else href="/dashboard" @endif>
          <i class="fas fa-fw fa-chart-line"></i>
          <span>Dashboard</span></a>

      </li>
      {{-- HOST --}}
      @if(Auth::user()->role=='admin')
      <li class="nav-item {{ Request::segment(1) === 'kelola_user' ? 'active' : null }}">
        <a class="nav-link" href="/kelola_user">
          <i class="fas fa-fw fa-user"></i>
          <span>Kelola User</span></a>
      </li>
      <li class="nav-item {{ Request::segment(2) === 'transaction' ? 'active' : null }}">
        <a class="nav-link" href="/admin/transaction">
          <i class="fas fa-fw fa-wallet"></i>
          <span>Kelola Transaksi</span></a>
      </li>

      @elseif(Auth::user()->role=='host')
      <!-- Nav Item - Kelola Petualangan -->
      <li class="nav-item {{ Request::segment(1) === 'kelola_petualangan' ? 'active' : null }}">
        <a class="nav-link" href="/kelola_petualangan">
          <i class="fas fa-fw fa-hiking"></i>
          <span>Kelola Petualangan</span></a>
      </li>

      <!-- Nav Item - Kelola Pemesanan -->
      <li class="nav-item {{ Request::segment(2) === 'transaction' ? 'active' : null }}">
        <a class="nav-link" href="/host/transaction">
          <i class="fas fa-fw fa-wallet"></i>
          <span>Kelola Pemesanan</span></a>
      </li>
      @endif

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    {{-- Content wrapper --}}
    <div class="d-flex flex-column" id="content-wrapper">
      <div id="content">
        <nav class="navbar static-top navbar-expand-md mb-4 navbar-dark bg-twalang shadow-sm">
          <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
          </a>
          <button class="navbar-toggler mb-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              @if(Request::is('/'))
              @else
              <li class="nav-item mr-3">
                <form id="formSearch" name="formSearch" action="/search/result" class="form-inline">
                  <div class="input-group">
                    <input name="keyword" form="formSearch" type="text" class="form-control bg-light border-0 small"
                      placeholder="Cari petualangan..." aria-label="Search" aria-describedby="basic-addon2"
                      @if(Request::segment(1)==='search' ) value="{{$target['keyword']}}" @endif>
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit" form="formSearch">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                  @if(Request::segment(1) != 'search')
                  <input type="text" name="kota" hidden>
                  <input type="text" name="jenis_petualangan" hidden>
                  @endif
                </form>
              </li>
              @endif
            </ul>

            <ul class="navbar-nav ml-auto">
              {{-- Bisa walaupun keduanya --}}


              <li class="nav-item">
                <a @guest href="#" onclick="loginModal()" @else @if(Auth::user()->role=='host')
                  href="/experiences/create"
                  @else
                  href="/hosts/create"
                  @endif
                  @endguest class="nav-link"><strong>{{ __('Buat Petualangan') }}</strong></a>
              </li>
              <!-- Authentication Links -->
              @guest
              <li class="nav-item">
                <a class="nav-link" href="#" onclick="loginModal()">{{ __('Masuk') }}</a>
              </li>
              @if (Route::has('register'))
              <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Daftar') }}</a>
              </li>
              @endif
              @else
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                  aria-haspopup="true" aria-expanded="false">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                {{-- dropdown --}}
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                  aria-labelledby="navbarDropdown">
                  {{-- Dropdown untuk kelola host --}}
                  <a class="dropdown-item" href="/dashboard" onclick="">
                    {{ __('Kelola Host') }}
                  </a>
                  {{-- Dropdown untuk logout --}}
                  <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                       document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
              @endguest
            </ul>
          </div>
        </nav>

        <!-- Begin Page Content -->
        <div class="container-fluid px-4">

          @yield('content')

        </div>
        <!-- /.container-fluid -->

      </div>
    </div>

  </div>





  {{-- JS --}}
  @yield('dashboard-scripts')
  @yield('dashboard-admin-scripts')
  <!-- Bootstrap core JavaScript-->
  <script src="{{ URL::asset('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ URL::asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ URL::asset('js/sb-admin-2.min.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{URL::asset('vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{URL::asset('js/demo/chart-area-demo.js')}}"></script>
  <script src="{{URL::asset('js/demo/chart-bar-demo.js')}}"></script>
  <script src="{{URL::asset('js/demo/chart-pie-demo.js')}}"></script>
  <script src="{{URL::asset('js/demo/datatables-demo.js')}}"></script>
  <script src="{{URL::asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{URL::asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script type="text/javascript">
    (() => {
    'use strict';
        const objects = document.getElementsByClassName('asyncImage');
        Array.from(objects).map((item) => {
            const img = new Image();
            img.src = item.dataset.src;
            img.onload = () => {
                item.classList.remove('asyncImage');
                return item.nodeName === 'IMG' ?
                    item.src = item.dataset.src :
                    item.style.backgroundImage = `url(${item.dataset.src})`;
            };
        });
    })();
  </script>
  <script type="text/javascript">
    window.setTimeout(function() {
      $(".alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove(); 
      });
    }, 2000);
    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#foto-aktivitas')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    window.setTimeout(function() {
      $(".alert").fadeTo(500, 0).slideUp(500, function(){
          $(this).remove(); 
      });
    }, 2000);
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#foto-experience')
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function readURLEditAktivitas(input, num) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var idName = '#' + num;
                $(idName)
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>
  @include('includes.scripts')
</body>

</html>
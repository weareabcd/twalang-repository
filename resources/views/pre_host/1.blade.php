@extends('layouts.pre_host')

@section('content')

    <div class="container-fluid">

        <div class="row align-items-center content-justify-center text-center" style="height:100vh;background-color:#f7fafc">
            <div class="col-md-6 pre-hosting-1-image d-flex" style="background-image:url({{ asset('img/pre-hosting-1.jpg')}})">
                
            </div>
            <div class="col-md-6 pre-hosting-caption">
                
                    <div class="row my-3 justify-content-center text-left">
                        <div class="col-md-10">
                            <h1><strong>Ajak tamu berpetualang ke dalam duniamu yang seru</strong></h1>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-center text-left">
                        <div class="col-md-10">
                            dengan memiliki petualangan untuk ditawarkan di Twalang, kamu akan memandu tamu merasakan pengalaman mengesankan dari dunia yang kamu sukai sekaligus kuasai
                        </div>
                    </div>
                    <div class="row my-3 justify-content-center text-left">
                        <div class="col-md-10">
                            <a href="/pre/2" class="btn btn-outline-primary">Selanjutnya</a>
                        </div>
                    </div>
                    <div class="row my-3 justify-content-center text-left">
                        <div class="col-md-10">
                            <a href="/"><small>< Kembali</small></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>


@endsection

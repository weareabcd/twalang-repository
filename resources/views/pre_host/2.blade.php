@extends('layouts.pre_host')

@section('content')

    <div class="container-fluid">

        <div class="row align-items-center justify-content-center" style="height:100vh;background-color:#f7fafc">
            <div class="col-md-6 pre-hosting-1-image d-flex" style="background-image:url('{{ asset('img/pre-hosting-2.jpg') }}')">

            </div>
            <div class="col-md-6 pre-hosting-caption">
                
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <h1><strong>Memperoleh keuntungan dengan menerima tamu untuk dipandu</strong></h1>
                    </div>
                </div>
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        Anda bebas menjual petualangan kepada para tamu, dengan memperhatikan ketentuan standar yang sudah Twalang tetapkan
                    </div>
                </div>
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <a href="/pre/3" class="btn btn-outline-primary">Selanjutnya</a>
                    </div>
                </div>
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <a href="/pre/1"><small>< Kembali</small></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
@endsection
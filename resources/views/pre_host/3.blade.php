@extends('layouts.pre_host')

@section('content')

    <div class="container-fluid">

        <div class="row align-items-center justify-content-center" style="height:100vh;background-color:#f7fafc">
            <div class="col-md-6 pre-hosting-1-image d-flex" style="background-image:url('{{ asset('img/pre-hosting-3.jpg') }}')">
                    
            </div>
            <div class="col-md-6 pre-hosting-caption">
                
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <h1><strong>Siap untuk memandu tamu di petualanganmu?</strong></h1>
                    </div>
                </div>
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <a href="/hosts/create" class="btn btn-outline-primary btn-block">Buat Petualangan!</a>
                    </div>
                </div>
                <div class="row my-3 justify-content-center text-left">
                    <div class="col-md-10">
                        <a href="/pre/2"><small>< Kembali</small></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
@endsection
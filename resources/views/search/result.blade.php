@extends('layouts.app')

@section('content')
    
    <div class="container" style="margin-top:56px;">
        <div class="row pt-4 justify-content-center">
            <div class="col-md-10" style="color:#0c3f6b">
                <h2><strong>Menemukan Petualangan Terbaik Untukmu</strong></h2>
            </div>
        </div>

        <div class="row mb-3 justify-content-center align-items-center">
            {{-- <div class="col-md-8" style="color:#0c3f6b">
            {{  $search_title  }}
            </div> --}}
            {{-- <div class="col-md-2 d-flex justify-content-end">
                <button onclick="editSearch()" class="btn btn-outline-primary btn-sm">
                    Ubah pencarian
                </button>
            </div>  --}}
            <div class="col-md-10 mt-3" style="" id="editSearch">
                
                    {{-- <form id="formSearch" name="formSearch" action="/search/result" class="form-inline"> --}}
                    <div class="form-inline">

                    
                        {{-- <div class="form-group mx-3">
                            <label for="">Mencari</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Ketik sesuatu">
                        </div> --}}
                        {{-- <div class="form-group mx-3">
                            <label for="">di</label>
                        </div> --}}
                        {{-- <input type="text" name="keyword" hidden> --}}
                        <div class="form-group mr-2">
                            {{-- <label for="kota" class="sr-only">Kota</label> --}}
                            <select onchange="cariSubmit()" name="kota" id="kota" class="form-control" form="formSearch">
                                    <option value="" >Semua kota</option>
                                @foreach ($locs as $loc)
                                    <option @if($target['kota']==$loc->id_location) selected @endif value="{{ $loc->id_location }}">{{ $loc->nama_location }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="form-group mx-sm-3">
                            <label for="">dalam kategori</label>
                        </div> --}}
                        <div class="form-group">
                            <label for="jenis_petualangan" class="sr-only">Kategori</label>
                            <select form="formSearch" onchange="cariSubmit()" name="jenis_petualangan" id="jenis_petualangan" class="form-control">
                                    <option value="">Semua kategori</option>
                                @foreach ($cats as $cat)
                                    <option @if($target['jenis_petualangan']==$cat->id_category) selected @endif value="{{ $cat->id_category }}">{{ $cat->nama_category }}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="form-group mx-sm-3">
                            <button type="submit" class="btn btn-success">Cari lagi</button>
                        </div> --}}
                    {{-- </form> --}}
                    </div>
            </div>
        </div>
        
            
        <div class="row">
            <div class="col-12">
                @foreach ($results as $result)
                    <a href="/experiences/show/{{$result->id_experience}}" target="_blank" class="card mx-auto my-2 card-experience petualangan col-md-10" style="text-decoration:none; color:#363a40;">
                        <div class="card-body" style="">
                            <div class="row">
                                <div class="col-md-4">
                                    <img data-src="{{$result->experience_photos->first()['link']}}" src="{{asset('img/loading-image.gif')}}" class="card-img asyncImage" style="height:220px;object-fit: cover;">
                                </div>
                                <div class="col-md-8">
                                    <div class="row" >
                                        <div class="col">
                                            <h4><strong>{{ $result->nama_experience }}</strong></h4>
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="col">
                                            <i class="fas fa-map-pin mr-2"></i>{{ $result->location->nama_location }}
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="col">
                                            {{ $result->deskripsi }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mt-3" >
                                               <h3><strong>@currency( $result->harga )</strong></h3>                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        
    </div>

@endsection

@section('scripts')
    <script>
    
        function editSearch()
        {
            var x = document.getElementById('editSearch');
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
                }
        }

        function hideEditSearch()
        {
            var x = document.getElementById('editSearch')
            x.style.display = "none";
        }

    </script>
@endsection
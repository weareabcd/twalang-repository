<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/experiences/show/{experience}','ExperienceController@show');
Auth::routes();

Route::get('/search/result','SearchController@result');

Route::group(['middleware' => ['auth', 'checkRole:host']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    //Host
    Route::get('/dashboard', 'HostController@dashboard');
    Route::get('/dashboard/time/{time}', 'HostController@dashboardSortByTime');
    Route::get('/kelola_petualangan', 'HostExperienceController@index');
    Route::get('/kelola_petualangan/show/{experience}', 'HostExperienceController@show');
    Route::post('/kelola_petualangan/edit/{experience}', 'HostExperienceController@update');
    Route::post('/kelola_petualangan/show/{experience}/activity/create', 'ExperienceActivityController@store');
    Route::patch('/kelola_petualangan/show/{experience}/activity/{activity}/edit', 'ExperienceActivityController@update');
    Route::delete('/kelola_petualangan/show/{experience}/activity/{activity}/delete', 'ExperienceActivityController@destroy');
    //Experience
    Route::get('/experiences/create', 'ExperienceController@create');
    Route::post('/experience/create', 'ExperienceController@store');
    Route::get('/experience/create/new', function () { return view('experiences.create-experience');});
    //Photos
    Route::post('/kelola_petualangan/{experience}/experience_photos/create', 'ExperiencePhotoController@store');
    Route::delete('/kelola_petualangan/{experience}/experience_photos/{experience_photo}/delete', 'ExperiencePhotoController@destroy');
    //Transaction
    Route::get('/host/transaction','HostController@transaction');

    Route::post('/experience/payment','Payment@pay');
    Route::post('/transaction/submit','Payment@inputTransaction');
});

Route::group(['middleware' => ['auth', 'checkRole:guest']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    //Menjadi Host
    Route::get('/pre/1', function () { return view('pre_host.1');});
    Route::get('/pre/2', function () { return view('pre_host.2');});
    Route::get('/pre/3', function () { return view('pre_host.3');});
    Route::get('/hosts/create', 'HostController@create');
    Route::post('/hosts/create', 'HostController@store');
    Route::get('/hosts/create/new', function () { return view('hosts.create-host');});

    Route::post('/experience/payment','Payment@pay');
    Route::post('/transaction/submit','Payment@inputTransaction');

    Route::get('/user/experience','PagesController@upcoming');
    Route::post('/rate/{id}', 'ExperienceController@rate');
});

Route::group(['middleware' => ['auth', 'checkRole:admin']], function(){
    Route::get('/dashboard/admin', 'AdminController@dashboard');
    Route::get('/kelola_user', 'AdminController@kelola_user');
    Route::get('/admin/transaction','AdminController@kelola_transaksi');
    Route::post('/admin/transaction/{transaction}/update','payment@adminSetStatus');
});